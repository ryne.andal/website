name := "fakebaseballwebsite"

version := "1.0"

lazy val `fakebaseballwebsite` = (project in file(".")).enablePlugins(PlayScala, BuildInfoPlugin)
lazy val `flywayrunner` = project.settings(
  libraryDependencies ++= Seq(
    "mysql" % "mysql-connector-java" % "8.0.12",
    "org.flywaydb" % "flyway-core" % "5.1.4"
  ),
  flywayUrl := (configFile in fakebaseballwebsite).value.getString("slick.dbs.default.db.url"),
  flywayUser := (configFile in fakebaseballwebsite).value.getString("slick.dbs.default.db.user"),
  flywayPassword := (configFile in fakebaseballwebsite).value.getString("slick.dbs.default.db.password"),
  flywayLocations := Seq("filesystem:evolutions/")
).enablePlugins(FlywayPlugin)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

buildInfoKeys := Seq(
  name,
  version,
  scalaVersion,
  sbtVersion,
  BuildInfoKey.action("buildTime") {
    System.currentTimeMillis
  },
  BuildInfoKey.action("gitSha") {
    import sys.process._
    try {
      "git rev-parse --short HEAD".!!.trim
    } catch {
      case _: Exception => "Unknown"
    }
  }
)

lazy val excludedWatchSources = settingKey[Seq[String]]("The list of excluded files from Play!'s file watcher")
excludedWatchSources := Seq("Tables.scala", "versions", "BuildInfo.scala") // These files are generated on build, so including them in watchSources causes multiple reloads per request in Play!
watchSources := watchSources.value.filter { source => !excludedWatchSources.value.contains(source.base.getName) }

libraryDependencies ++= Seq(guice, ws)

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.12",
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "org.jsoup" % "jsoup" % "1.11.3",
  "ai.x" %% "play-json-extensions" % "0.40.2"
)

libraryDependencies ++= Seq(
  "com.atlassian.commonmark" % "commonmark" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-autolink" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-gfm-strikethrough" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-gfm-tables" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-heading-anchor" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-ins" % "0.12.1",
)


unmanagedResourceDirectories in Test += {
  baseDirectory(_ / "target/web/public/test").value
}

// Reads config file for db migration and slick-codegen
import com.typesafe.config.{Config, ConfigFactory}

lazy val configFile = settingKey[Config]("Application configuration")
configFile := ConfigFactory.load(ConfigFactory.parseFile {
  val productionConf = new File("conf/production.conf")
  if (productionConf.exists)
    productionConf
  else {
    val env = System.getenv("config.file")
    val resource = System.getenv("config.resource")
    if (env != null)
      new File(env)
    else if (resource != null)
      new File("conf/" + resource)
    else
      new File("conf/development.conf")
  }
})

// Slick DB structure code generation, runs on every compile
lazy val codegen = taskKey[Unit]("Generates Slick tables")
codegen := {
  import slick.codegen.SourceCodeGenerator
  import slick.jdbc.MySQLProfile
  import slick.jdbc.MySQLProfile.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  sLog.value.info("Generating Tables file....")
  val db = Database.forConfig("slick.dbs.default.db", configFile.value)

  val modelAction = MySQLProfile.createModel(Some(MySQLProfile.defaultTables))
  val modelFuture = db.run(modelAction)

  val codegenFuture = modelFuture.map(model => new SourceCodeGenerator(model) {
    override def Table = new Table(_) {
      override def hugeClassEnabled: Boolean = false
    }
  })

  codegenFuture.foreach(codegenInfo => codegenInfo.writeToFile("slick.jdbc.MySQLProfile", "./app/", "model", "Tables", "Tables.scala"))
  sLog.value.info("Generated Tables file.")
}
codegen := (codegen dependsOn (flywayMigrate in flywayrunner)).value
(compile in Compile) := ((compile in Compile) dependsOn codegen).value
(doc in Compile) := ((doc in Compile) dependsOn (compile in Compile)).value
