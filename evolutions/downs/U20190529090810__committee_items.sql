DROP TABLE IF EXISTS committee_item_votes;
DROP TABLE IF EXISTS committee_item_motioners;
DROP TABLE IF EXISTS committee_items;

ALTER TABLE users
    ADD COLUMN is_committee BOOL NOT NULL;
