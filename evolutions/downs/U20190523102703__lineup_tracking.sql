ALTER TABLE games
    DROP FOREIGN KEY game_away_lineup,
    DROP FOREIGN KEY game_home_lineup,
    DROP COLUMN away_lineup,
    DROP COLUMN home_lineup;

DROP TABLE IF EXISTS lineup_entries;
DROP TABLE IF EXISTS lineups;
