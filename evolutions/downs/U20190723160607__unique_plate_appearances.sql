-- Adds a unique constraint on game actions
-- to prevent the same PA being logged twice
-- if they are entered in rapid succession

alter table game_actions
    drop index unique_game_actions_pa;