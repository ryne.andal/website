ALTER TABLE game_actions
    ADD COLUMN runs_scored INT NOT NULL,
    ADD COLUMN outs_tracked INT NOT NULL;
