alter table games
    add column thread_owner int null after id36,
    add foreign key fk_games_thread_owner (thread_owner) references users (id);
