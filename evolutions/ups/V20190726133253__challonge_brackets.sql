create table challonge_brackets (
    id varchar(100) not null primary key,
    theme int not null default 1,
    multiplier float not null default 1,
    match_width_multiplier float not null default 1,
    scale_to_fit bool not null default false,
    show_final_results bool not null default false,
    show_standings bool not null default false,
    show_voting bool not null default false,
    tab varchar(32) null
);