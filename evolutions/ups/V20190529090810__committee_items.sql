ALTER TABLE users
    ADD COLUMN is_committee BOOL NOT NULL DEFAULT FALSE;

CREATE TABLE IF NOT EXISTS committee_items
(
    id         INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title      VARCHAR(200) NOT NULL,
    body       TEXT         NOT NULL,
    is_private BOOL         NOT NULL,
    author     INT          NOT NULL,
    status     INT          NOT NULL, -- 0 for under review, 1 for voting, 2 for approved, 3 for rejected
    FOREIGN KEY fk_committee_items_author (author) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS committee_item_motioners
(
    committee_item INT NOT NULL,
    voter          INT NOT NULL,
    PRIMARY KEY (committee_item, voter),
    FOREIGN KEY fk_committee_item_motioners_committee_item (committee_item) REFERENCES committee_items (id) ON DELETE CASCADE,
    FOREIGN KEY fk_committee_item_motioners_voter (voter) REFERENCES users (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS committee_item_votes
(
    committee_item INT  NOT NULL,
    voter          INT  NOT NULL,
    voted_for      BOOL NOT NULL,
    PRIMARY KEY (committee_item, voter),
    FOREIGN KEY fk_committee_item_votes_committee_item (committee_item) REFERENCES committee_items (id) ON DELETE CASCADE,
    FOREIGN KEY fk_committee_item_votes_voter (voter) REFERENCES users (id) ON DELETE CASCADE
);
