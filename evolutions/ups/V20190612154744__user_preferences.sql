CREATE TABLE IF NOT EXISTS user_preferences (
    user INT NOT NULL PRIMARY KEY,
    ump_batter_ping TEXT NULL,
    FOREIGN KEY user_preferences_user (user) REFERENCES users(id)
);

INSERT INTO user_preferences (user) SELECT id FROM users;
