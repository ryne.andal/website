create table game_awards (
    game int not null primary key,
    winning_pitcher int not null,
    losing_pitcher int not null,
    player_of_the_game int not null,
    save int null,
    foreign key fk_game_awards_game (game) references games (id),
    foreign key fk_game_awards_winning_pitcher (winning_pitcher) references players (id),
    foreign key fk_game_awards_losing_pitcher (losing_pitcher) references players (id),
    foreign key fk_game_awards_player_of_the_game (player_of_the_game) references players (id),
    foreign key fk_game_awards_save (save) references players (id)
);