ALTER TABLE game_actions
    ADD COLUMN result_away_batting_position INT NOT NULL AFTER swing,
    ADD COLUMN result_home_batting_position INT NOT NULL AFTER result_away_batting_position;
