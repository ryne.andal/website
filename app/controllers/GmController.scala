package controllers
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class GmController @Inject() (implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def freeAgents:Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER){ implicit ru =>
    Ok(views.html.gm.freeagents(db.getFreeAgents))
  }

}
