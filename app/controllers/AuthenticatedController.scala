package controllers

import model._
import model.Tables.UsersRow
import play.api.mvc._
import services.GlobalSettingsProvider

import scala.language.implicitConversions

abstract class AuthenticatedController(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AbstractController(cc) {

  implicit def tuple2Request(tuple: (MessagesRequest[AnyContent], UsersRow)): MessagesRequest[AnyContent] = tuple._1
  implicit def impTuple2Request(implicit tuple: (MessagesRequest[AnyContent], UsersRow)): MessagesRequest[AnyContent] = tuple._1
  implicit def tuple2User(tuple: (MessagesRequest[AnyContent], UsersRow)): UsersRow = tuple._2
  implicit def impTuple2User(implicit tuple: (MessagesRequest[AnyContent], UsersRow)): UsersRow = tuple._2

  type Scope = UsersRow => Boolean
  val SCOPE_PLAYER: Scope = _.isPlayer
  val SCOPE_UMPIRE: Scope = _.isUmpire
  val SCOPE_COMMISSIONER: Scope = _.isCommissioner
  val SCOPE_COMMITTEE: Scope = _.isCommittee
  val SCOPE_GM: Scope = _.isGm
  implicit class ScopeExtensions(scope: Scope) {
    def ||(otherScope: Scope): Scope = u => scope(u) || otherScope(u)
    def &&(otherScope: Scope): Scope = u => scope(u) && otherScope(u)
  }

  def user(implicit req: Request[AnyContent]): Option[User] = {
    req.session.get("uid").map(_.toInt).flatMap { id =>
      db.getUserById(id)
    }
  }

  def UnauthorizedPage(implicit req: MessagesRequest[AnyContent], user: User): Result = Unauthorized(views.html.errors.unauthorized())

  def UserAuthenticatedAction(requiredScopes: Scope*)(body: ((MessagesRequest[AnyContent], UsersRow)) => Result): Action[AnyContent] = messagesActionBuilder { implicit req: MessagesRequest[AnyContent] =>
    user.map { user =>
      if (requiredScopes.forall(_(user)))
        body(req, user)
      else
        UnauthorizedPage(req, user)
    } getOrElse {
      Redirect(routes.AuthController.redirectToSignIn(req.path, true))
    }
  }

}
