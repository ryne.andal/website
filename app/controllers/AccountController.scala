package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.forms.UserPreferencesForm
import model.forms.UserPreferencesForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{GlobalSettingsProvider, RedditAuthService}

@Singleton
class AccountController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents, redditAuthService: RedditAuthService) extends AuthenticatedController {

  def myAccount: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Redirect(routes.AccountController.viewAccount(ru._2.id))
  }

  def viewAccount(id: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.getUserWithPreferencesById(id).filter(_.user.id == ru._2.id || ru.isCommissioner).map { up =>
      val prefs = up.preferences.withDefaults
      val currentPrefs = UserPreferencesForm(prefs.umpBatterPing)
      Ok(views.html.account.view(up.user, db.getPlayerForUser(up.user), userPreferencesForm.fill(currentPrefs).discardingErrors))
    } getOrElse {
      Unauthorized("You are not permitted to view this.")
    }
  }

  def submitPreferences(id: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.getUserWithPreferencesById(id).filter(_.user.id == ru._2.id).map { up =>
      userPreferencesForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.account.view(up.user, db.getPlayerForUser(up.user), formWithErrors)),
        formData => {
          db.updateUserPreferences(id, formData)
          Redirect(routes.AccountController.viewAccount(id))
        }
      )
    } getOrElse {
      Unauthorized("You are not permitted to view this.")
    }
  }

  def clearDiscord: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.setDiscordSnowflake(ru, None)
    Redirect(routes.AccountController.viewAccount(ru._2.id))
  }

  def clearRedditRefresh: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    ru.refreshToken.foreach(redditAuthService.revokeToken(_, "refresh_token"))
    db.setRefreshToken(ru._2.id, None)
    Redirect(routes.AccountController.viewAccount(ru._2.id))
  }

  def clearGoogle: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.setGoogleId(ru._2, None)
    Redirect(routes.AccountController.viewAccount(ru._2.id))
  }

}
