package controllers

import java.text.SimpleDateFormat
import java.util.Date

import buildinfo.BuildInfo
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.forms.AssignPlayerToTeamForm._
import model.forms.CreatePlayerForm._
import model.forms.EditParkForm._
import model.forms.EditPlayerForm._
import model.forms.EditTeamForm._
import model.forms.EndGameForm._
import model.forms.NewGameForm._
import model.forms.SetGmForm._
import model.forms.UmpireAssignmentForm._
import model.forms.UmpireStatusForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import play.api.{Environment, Mode}
import services.{DiscordMessageService, GlobalSettingsProvider}

import scala.util.Try

@Singleton
class AdminController @Inject()(env: Environment, discordMessageService: DiscordMessageService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  lazy val versions: (String, String, String) = ((if (env.mode == Mode.Dev) "Dev-" else "") + BuildInfo.gitSha, db.getDBMigrationVersion, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(BuildInfo.buildTime)))

  def adminPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.index(db.countPendingPlayerApplications(), versions))
  }

  def listPendingApplications: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.applications(db.getAllPendingApplications))
  }

  def showNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    Ok(views.html.admin.newgame(teams, newGameForm, db.getAllParks, mlr))
  }

  def submitNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    newGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.newgame(teams, formWithErrors, db.getAllParks, mlr)),
      formData => {
        db.createGame(formData)
        Redirect(routes.AdminController.showNewGameForm(mlr))
      }
    )
  }

  def showUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.manageumpires(db.getAllActiveUmpires, umpireStatusForm))
  }

  def submitUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireStatusForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.manageumpires(db.getAllActiveUmpires, formWithErrors)),
      formData => {
        db.setUmpireStatus(db.getUserByRedditName(formData.user).get.id, formData.newUmpStatus)
        Redirect(routes.AdminController.showUmpireStatusForm())
      }
    )
  }

  def showUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnfinishedGames, db.getGamesWithUmpAssignments, umpireAssignmentForm))
  }

  def submitUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireAssignmentForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnfinishedGames, db.getGamesWithUmpAssignments, formWithErrors)),
      formData => {
        val ump = db.getUserByRedditName(formData.umpire).get
        if (formData.addition)
          db.assignUmpireToGame(ump, formData.game)
        else
          db.removeUmpireFromGame(ump, formData.game)
        Redirect(routes.AdminController.showUmpireAssignmentForm())
      }
    )
  }

  def viewEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.endgame(db.getActiveGames, endGameForm))
  }

  def submitEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    endGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.endgame(db.getActiveGames, formWithErrors)),
      formData => {
        db.markGameComplete(formData.gameId)
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def showEditPlayerDirectory: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editplayerlist(db.getAllPlayerNames))
  }

  def showEditPlayerForm(id: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    id.flatMap(db.getPlayerWithTypes).map { player =>
      Ok(views.html.admin.editplayer(player, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses, editPlayerForm))
    } getOrElse {
      NotFound("This player does not exist.")
    }
  }

  def submitEditPlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editPlayerForm.bindFromRequest.fold(
      formWithErrors => {
        formWithErrors.data.get("id").flatMap(idStr => Try(idStr.toInt).toOption).flatMap { id =>
          db.getPlayerWithTypes(id).map { player =>
            BadRequest(views.html.admin.editplayer(player, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses, formWithErrors))
          }
        } getOrElse {
          BadRequest(views.html.admin.editplayerlist(db.getAllPlayerNames))
        }
      },
      formData => {
        // Edit player in DB
        db.editPlayer(formData)
        // Return to main admin page
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def showCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.createplayer(createPlayerForm, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses))
  }

  def submitCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    createPlayerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.createplayer(formWithErrors, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses)),
      formData => {
        val user = db.getUserByRedditName(formData.redditName).map(_.id).getOrElse(db.createUserAccount(formData.redditName))
        val id = db.createPlayer(user, formData)
        Redirect(routes.PlayerController.showPlayer(id))
      }
    )
  }

  def showEditParkSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editparkselector(db.getAllParks))
  }

  def showEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val park = parkId.flatMap(db.getParkById)
    park.map { park =>
      val form = editParkForm.bind(Map(
        "name" -> park.name,
        "factorHR" -> park.factorHr.toString,
        "factor3B" -> park.factor3b.toString,
        "factor2B" -> park.factor2b.toString,
        "factor1B" -> park.factor1b.toString,
        "factorBB" -> park.factorBb.toString,
      ))
        .discardingErrors
      Ok(views.html.admin.editpark(parkId, form))
    } getOrElse {
      Ok(views.html.admin.editpark(parkId, editParkForm))
    }
  }

  def submitEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editParkForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editpark(parkId, formWithErrors)),
      formData => {
        val newId: Int = parkId.map { parkId =>
          db.editPark(parkId, formData)
          parkId
        } getOrElse {
          db.createPark(formData)
        }
        Redirect(routes.AdminController.showEditParkForm(Some(newId)))
      }
    )
  }

  def showEditTeamSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.teamselector(db.getAllTeams, routes.AdminController.showEditTeamForm(None), showNew = true))
  }

  def showEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val team = teamId.flatMap(db.getTeamById)
    team.map { team =>
      val form = editTeamForm.bind(Map(
        "tag" -> team.tag,
        "name" -> team.name,
        "park" -> team.park.toString,
        "discordRole" -> team.discordRole.getOrElse(""),
        "colorDiscord" -> f"#${team.colorDiscord}%06x",
        "colorRoster" -> f"#${team.colorRoster}%06x",
        "colorRosterBg" -> f"#${team.colorRosterBg}%06x",
        "milr" -> team.milr.toString,
        "milrTeam" -> team.milrTeam.map(_.toString).getOrElse("")
      ))
        .discardingErrors
      Ok(views.html.admin.editteam(Some(team.id), form, db.getAllParks, db.getMiLRTeams))
    } getOrElse {
      Ok(views.html.admin.editteam(None, editTeamForm, db.getAllParks, db.getMiLRTeams))
    }
  }

  def submitEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editTeamForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editteam(teamId, formWithErrors, db.getAllParks, db.getMiLRTeams)),
      formData => {
        val newId: Int = teamId.map { teamId =>
          db.editTeam(teamId, formData)
          teamId
        } getOrElse {
          db.createTeam(formData)
        }
        Redirect(routes.AdminController.showEditTeamForm(Some(newId)))
      }
    )
  }

  def redirectToCurrentGameHealthTable: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val maxSession = db.getMaxSession
    Redirect(routes.AdminController.showGameHealthTable(maxSession._1, maxSession._2))
  }

  def showGameHealthTable(season: Int, session: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.umpcheck(db.getGameStatusInfo(season, session), (season, session), db.getSessionCounts))
  }

  def showSetGmTeamSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.teamselector(db.getAllTeams, routes.AdminController.showSetGmForm(0)))
  }

  def showSetGmForm(teamId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getTeamById(teamId).map { team =>
      val gma = db.getGMOfTeam(team)
      Ok(views.html.admin.setgm(team, gma, setGmForm, db.getPlayersOnTeam(team)))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitSetGmForm(teamId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getTeamById(teamId).map { team =>
      setGmForm.bindFromRequest().fold(
        formWithErrors => BadRequest(views.html.admin.setgm(team, db.getGMOfTeam(team), formWithErrors, db.getPlayersOnTeam(team))),
        formData => {
          db.assignGMToTeam(team, formData.newGM, formData.oldGMReason, formData.season, formData.session)
          Redirect(routes.AdminController.showSetGmForm(teamId))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showTeamAssignmentForm(player: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getPlayerById(player).map { player =>
      Ok(views.html.admin.assigntoteam(player, db.getAllTeams, assignPlayerToTeamForm))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitTeamAssignmentForm(player: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getPlayerById(player).map { player =>
      assignPlayerToTeamForm.bindFromRequest().fold(
        formWithErrors => BadRequest(views.html.admin.assigntoteam(player, db.getAllTeams, formWithErrors)),
        formData => {
          db.assignPlayerToTeam(player, formData)

          // Update discord roles if team changed
          if (player.team != formData.team.map(_.id)) {
            db.getUserById(player.user).get.discord.foreach { discordSnowflake =>
              // Remove old role
              val oldRole = player.team
                .flatMap(db.getTeamById)
                .flatMap(_.discordRole)
                .getOrElse(settingsProvider.DISCORD_FREE_AGENT_ROLE.get)
              discordMessageService.roleUser(discordSnowflake, oldRole, remove = true)
              // Add new role
              val newRole = formData.team
                .flatMap(_.discordRole)
                .getOrElse(settingsProvider.DISCORD_FREE_AGENT_ROLE.get)
              discordMessageService.roleUser(discordSnowflake, newRole, remove = false)
            }
          }
          Redirect(routes.PlayerController.showPlayer(player.id))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

}
