package controllers

import javax.inject.{Inject, Singleton}
import model.forms.ChallongeBracketForm._
import model.FBDatabase
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class ChallongeController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def redirectToBracket: Action[AnyContent] = messagesActionBuilder {
    Redirect(routes.ChallongeController.viewChallongeBracket(db.getAllChallongeBracketNames.headOption.getOrElse("1")))
  }

  def viewChallongeBracket(id: String): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.challonge.view(db.getChallongeBracketById(id), db.getAllChallongeBracketNames)(req, user))
  }

  def showNewChallongeBracketForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.challonge.edit(challongeBracketForm))
  }

  def showEditChallongeBracketForm(id: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getChallongeBracketById(id).map { bracket =>
      val form = challongeBracketForm.bind(Map(
        "id" -> bracket.id,
        "theme" -> bracket.theme.toString,
        "multiplier" -> bracket.multiplier.toString,
        "matchWidthMultiplier" -> bracket.matchWidthMultiplier.toString,
        "scaleToFit" -> bracket.scaleToFit.toString,
        "showFinalResults" -> bracket.showFinalResults.toString,
        "showStandings" -> bracket.showStandings.toString,
        "showVoting" -> bracket.showVoting.toString,
        "tab" -> bracket.tab.getOrElse("")
      ))
          .discardingErrors
      Ok(views.html.challonge.edit(form))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitChallongeBracketForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    challongeBracketForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.challonge.edit(formWithErrors)),
      formData => {
        db.editOrCreateChallongeBracket(formData)
        Redirect(routes.ChallongeController.redirectToBracket())
      }
    )
  }

  def deleteChallongeBracket: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    challongeBracketForm.bindFromRequest.fold(
      _ => {},
      formData => db.deleteChallongeBracket(formData.id)
    )
    Redirect(routes.ChallongeController.redirectToBracket())
  }

}
