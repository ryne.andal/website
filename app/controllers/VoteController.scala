package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.forms.VoteForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class VoteController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def showVoteForm: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Ok(views.html.vote.closed())
//    if (db.getPlayerForUser(ru).isEmpty)
//      UnauthorizedPage
//    val form = db.getVote(ru._2.id).map { vote =>
//      voteForm
//        .bind(Map(
//          "option1" -> vote.option1,
//          "option2" -> vote.option2,
//          "option3" -> vote.option3,
//          "option4" -> vote.option4,
//          "option5" -> vote.option5,
//        ))
//    } getOrElse voteForm
//    Ok(views.html.vote.vote(form))
  }

  def submitVoteForm: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Ok(views.html.vote.closed())
//    voteForm.bindFromRequest.fold(
//      formWithErrors => BadRequest(views.html.vote.vote(formWithErrors)),
//      formData => {
//        db.submitVote(ru._2.id, formData)
//        Redirect(routes.VoteController.showPostVote())
//      }
//    )
  }

  def showPostVote: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.getVote(ru._2.id).map { vote =>
      Ok(views.html.vote.post(vote))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showTally: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.vote.tally(db.getVoteTally.toSeq.sortBy(_._2).reverse))
  }

}
