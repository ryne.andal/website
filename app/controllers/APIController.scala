package controllers

import javax.inject.{Inject, Singleton}
import model._
import play.api.libs.json._
import play.api.mvc._
import services.GlobalSettingsProvider

@Singleton
class APIController @Inject()(implicit  settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  private def apiAction(body: Request[AnyContent] => Result): Action[AnyContent] = Action { implicit req =>
    body(req)
      .enableCors
  }

  private def apiAction(body: => Result): Action[AnyContent] = apiAction(_ => body)

  def gamesInSession(season: Int, session: Int): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.getGamesInSession(season, session)))
  }

  def getActiveGameForTeam(team: String): Action[AnyContent] = apiAction {
    db.getActiveGameForTeam(team).map { game =>
      Ok(Json.toJson(game))
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def getGameLog(game: Int): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.getExtendedGameLog(game)))
  }

  def searchPlayer(name: String): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.searchPlayers(name)))
  }

  def getTeam(id: Int): Action[AnyContent] = apiAction {
    db.getTeamById(id).map { team =>
      Ok(Json.toJson(team))
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def searchTeam(query: String): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.searchTeams(query)))
  }

  def searchPark(query: String): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.searchParks(query)))
  }

  def playerBattingPlays(id: Int, milr: Option[Boolean]): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.getPlayerBattingPlays(id, milr)))
  }

  def playerPitchingPlays(id: Int, milr: Option[Boolean]): Action[AnyContent] = apiAction {
    Ok(Json.toJson(db.getPlayerPitchingPlays(id, milr)))
  }

  def playerFromSnowflake(snowflake: String): Action[AnyContent] = apiAction {
    db.getPlayerBySnowflake(snowflake).map { player =>
      Ok(Json.toJson(player))
    } getOrElse {
      NotFound("{\"error\": \"No player was found.\"}")
    }
  }

  def playersOnTeam(team: String): Action[AnyContent] = apiAction {
    db.getTeamByTag(team).map { team =>
      Ok(Json.toJson(db.getPlayersWithInfoOnTeam(team).sortWith(model.sortPlayersByPosition)))
    } getOrElse {
      Ok(Json.toJson(Seq[PlayerWithTypesAndInfo]()))
    }
  }

  def playerWithStats(id: Int, season: Option[Int], milr: Option[Boolean]): Action[AnyContent] = apiAction {
    db.getPlayerWithStatsById(id, season.getOrElse(db.getMaxSession._1), milr.getOrElse(false)).map { player =>
      Ok(Json.toJson(player))
    } getOrElse {
      NotFound("{\"error\": \"No player was found.\"}")
    }
  }

}
