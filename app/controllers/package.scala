import ai.x.play.json.Jsonx
import model.Tables._
import model._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes}
import play.api.mvc.Result

package object controllers {

  implicit class ResultExtensions(result: Result) {
    def enableCors: Result = result.withHeaders(
      "Access-Control-Allow-Origin" -> "*",
      "Access-Control-Allow-Methods" -> "GET, POST, PUT, PATCH, DELETE",
      "Access-Control-Allow-Headers" -> "Accept, Content-Type, Origin, X-Json, X-Prototype-Version, X-Requested-With",
      "Access-Control-Allow-Credentials" -> "true"
    )
  }

  // region JSON converters

  implicit val ParkWrites: Writes[Park] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "factorHR").write[Double] and
      (JsPath \ "factor3B").write[Double] and
      (JsPath \ "factor2B").write[Double] and
      (JsPath \ "factor1B").write[Double] and
      (JsPath \ "factorBB").write[Double]
    ) ((p: Park) => (p.id, p.name, p.factorHr, p.factor3b, p.factor2b, p.factor1b, p.factorBb))
  implicit val TeamWithParkWrites: Writes[TeamWithPark] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "tag").write[String] and
      (JsPath \ "park").write[Park] and
      (JsPath \ "colorDiscord").write[Int] and
      (JsPath \ "colorRoster").write[Int] and
      (JsPath \ "colorRosterBG").write[Int] and
      (JsPath \ "milr").write[Boolean] and
      (JsPath \ "milrTeam").write[Option[Int]]
    ) ((t: TeamWithPark) => (t.team.id, t.team.name, t.team.tag, t.park, t.team.colorDiscord, t.team.colorRoster, t.team.colorRosterBg, t.team.milr, t.team.milrTeam))
  implicit val TeamWrites: Writes[Team] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "tag").write[String] and
      (JsPath \ "park").write[Int] and
      (JsPath \ "colorDiscord").write[Int] and
      (JsPath \ "colorRoster").write[Int] and
      (JsPath \ "colorRosterBG").write[Int] and
      (JsPath \ "milr").write[Boolean] and
      (JsPath \ "milrTeam").write[Option[Int]]
    ) ((t: Team) => (t.id, t.name, t.tag, t.park, t.colorDiscord, t.colorRoster, t.colorRosterBg, t.milr, t.milrTeam))
  implicit val GameWrites: Writes[GameWithGameState] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "season").write[Int] and
      (JsPath \ "session").write[Int] and
      (JsPath \ "awayTeam").write[Team] and
      (JsPath \ "homeTeam").write[Team] and
      (JsPath \ "awayScore").write[Int] and
      (JsPath \ "homeScore").write[Int] and
      (JsPath \ "completed").write[Boolean] and
      (JsPath \ "outs").write[Int] and
      (JsPath \ "inning").write[String] and
      (JsPath \ "firstOccupied").write[Boolean] and
      (JsPath \ "secondOccupied").write[Boolean] and
      (JsPath \ "thirdOccupied").write[Boolean] and
      (JsPath \ "id36").write[Option[String]]
    ) ((g: GameWithGameState) => (g.game.id, g.game.season, g.game.session, g.awayTeam, g.homeTeam, g.state.scoreAway, g.state.scoreHome, g.game.completed, g.state.outs, g.inning, g.state.r1.isDefined, g.state.r2.isDefined, g.state.r3.isDefined, g.game.id36))
  implicit val BattingTypeWrites: Writes[BattingTypesRow] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (bt => (bt.id, bt.name, bt.shortcode, bt.rangeHr, bt.range3b, bt.range2b, bt.range1b, bt.rangeBb, bt.rangeFo, bt.rangeK, bt.rangePo, bt.rangeRgo, bt.rangeLgo))
  implicit val PitchingTypeWrites: Writes[PitchingTypesRow] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (pt => (pt.id, pt.name, pt.shortcode, pt.rangeHr, pt.range3b, pt.range2b, pt.range1b, pt.rangeBb, pt.rangeFo, pt.rangeK, pt.rangePo, pt.rangeRgo, pt.rangeLgo))
  implicit val PitchingBonusWrites: Writes[PitchingBonusesRow] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (pb => (pb.id, pb.name, pb.shortcode, pb.rangeHr, pb.range3b, pb.range2b, pb.range1b, pb.rangeBb, pb.rangeFo, pb.rangeK, pb.rangePo, pb.rangeRgo, pb.rangeLgo))
  implicit val PlayerWithTypesWrite: Writes[PlayerWithTypesAndInfo] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "discordSnowflake").write[Option[String]] and
      (JsPath \ "team").write[Option[Team]] and
      (JsPath \ "battingType").write[BattingType] and
      (JsPath \ "pitchingType").write[Option[PitchingType]] and
      (JsPath \ "pitchingBonus").write[Option[PitchingBonus]] and
      (JsPath \ "rightHanded").write[Boolean] and
      (JsPath \ "positionPrimary").write[String] and
      (JsPath \ "positionSecondary").write[Option[String]] and
      (JsPath \ "positionTertiary").write[Option[String]]
    ) (p => (p.player.id, p.player.name, p.user.redditName, p.user.discord, p.team, p.battingType, p.pitchingType, p.pitchingBonus, p.player.righthanded, p.player.positionPrimary, p.player.positionSecondary, p.player.positionTertiary))
  implicit val PlayerWithUserWrite: Writes[PlayerWithUser] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "redditName").write[String]
    ) (p => (p.player.id, p.player.name, p.user.redditName))
  val PlayerWithUserIncludingTypeIdsWrite: Writes[PlayerWithUser] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "battingType").write[Int] and
      (JsPath \ "pitchingType").write[Option[Int]] and
      (JsPath \ "pitchingBonus").write[Option[Int]] and
      (JsPath \ "rightHanded").write[Boolean]
  ) (p => (p.player.id, p.player.name, p.user.redditName, p.player.battingType, p.player.pitchingType, p.player.pitchingBonus, p.player.righthanded))
  implicit val GameWithTeamsWrite: Writes[GameWithTeams] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "season").write[Int] and
      (JsPath \ "session").write[Int] and
      (JsPath \ "awayTeam").write[Team] and
      (JsPath \ "homeTeam").write[Team] and
      (JsPath \ "id36").write[Option[String]]
    ) (t => (t.game.id, t.game.season, t.game.session, t.awayTeam, t.homeTeam, t.game.id36))
  implicit val GameStateWrite: Writes[GameState] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "awayScore").write[Int] and
      (JsPath \ "homeScore").write[Int] and
      (JsPath \ "outs").write[Int] and
      (JsPath \ "inning").write[String] and
      (JsPath \ "firstOccupied").write[Boolean] and
      (JsPath \ "secondOccupied").write[Boolean] and
      (JsPath \ "thirdOccupied").write[Boolean]
    ) ((gs: GameState) => (gs.id, gs.scoreAway, gs.scoreHome, gs.outs, gs.inningStr, gs.r1.isDefined, gs.r2.isDefined, gs.r3.isDefined))
  implicit val GameActionWithExtendedInfoWrite: Writes[GameActionWithExtendedInfo] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "game").write[GameWithTeams] and
      (JsPath \ "batter").write[PlayerWithUser] and
      (JsPath \ "swing").write[Option[Int]] and
      (JsPath \ "pitcher").write[PlayerWithUser] and
      (JsPath \ "pitch").write[Option[Int]] and
      (JsPath \ "diff").write[Option[Int]] and
      (JsPath \ "result").write[Option[String]] and
      (JsPath \ "beforeState").write[GameState] and
      (JsPath \ "afterState").write[GameState] and
      (JsPath \ "outsTracked").write[Int] and
      (JsPath \ "runsScored").write[Int] and
      (JsPath \ "scorers").write[Option[String]]
    ) (ga => (ga.gameAction.id, ga.game, ga.batter, ga.gameAction.swing, ga.pitcher, ga.gameAction.pitch, ga.gameAction.diff, ga.gameAction.result, ga.beforeState, ga.afterState,
    ga.gameAction.outsTracked, ga.gameAction.runsScored, ga.gameAction.scorers))
  implicit lazy val BattingStatSetWrite: Writes[BattingStatSet] = Jsonx.formatAuto[BattingStatSet]
  implicit lazy val PitchingStatSetWrite: Writes[PitchingStatSet] = Jsonx.formatAuto[PitchingStatSet]
  implicit lazy val PlayerWithStatsWrite: Writes[PlayerWithStats] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "battingStats").write[Option[BattingStatSet]] and
      (JsPath \ "pitchingStats").write[Option[PitchingStatSet]]
    ) (p => (p.player.id, p.player.name, p.user.redditName, p.battingStats, p.pitchingStats))

  // endregion

}
