package controllers

import javax.inject._
import model.FBDatabase
import play.api.mvc._
import play.twirl.api.Html
import services.{DiscordAuthService, GlobalSettingsProvider, GoogleAuthService, RedditAuthService}

import scala.collection.mutable
import scala.util.Random

@Singleton
class AuthController @Inject()(redditAuthService: RedditAuthService, discordAuthService: DiscordAuthService, googleAuthService: GoogleAuthService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  private val states = mutable.Set[String]()
  private val discordStates = mutable.Set[String]()
  private val googleStates = mutable.Set[String]()
  private val random = new Random()

  private def createNewState: String = {
    val randomInt = random.nextInt()
    val newState = Integer.toString(randomInt, 36)
    states.add(newState)
    newState
  }

  def redirectToSignIn(path: String, mobile: Boolean): Action[AnyContent] = Action { implicit req =>
    Redirect(redditAuthService.oauthConsentUri(createNewState, mobile))
      .addingToSession("redirection_target" -> path)
  }

  def signIn: Action[AnyContent] = Action { implicit req =>
    req.getQueryString("error").map { error =>
      BadRequest(s"Got an error from Reddit: $error\n\nPlease try again later.")
    } getOrElse {
      req.getQueryString("state").flatMap { state =>
        if (states.contains(state)) {
          states.remove(state)
          req.getQueryString("code").map { code =>
            val (accessToken, refreshToken) = redditAuthService.getOAuthTokens(code)
            val identity = redditAuthService.getIdentity(accessToken)
            val userId = db.getUserByRedditName(identity.name).map(_.id).getOrElse(db.createUserAccount(identity.name))
            refreshToken.map { refreshToken =>
              db.setRefreshToken(userId, Some(refreshToken))
              db.logSignIn(userId, "reddit_oauth_refresh", req.headers.get("X-Real-IP").getOrElse(req.remoteAddress))
              redditAuthService.revokeToken(accessToken, "access_token")
              Redirect(routes.AccountController.viewAccount(userId))
                .withSession("uid" -> userId.toString)
            } getOrElse {
              db.logSignIn(userId, "reddit_oauth", req.headers.get("X-Real-IP").getOrElse(req.remoteAddress))
              redditAuthService.revokeToken(accessToken, "access_token")
              Redirect(req.session.get("redirection_target").getOrElse("/"))
                .withNewSession
                .withSession("uid" -> userId.toString)
            }
          }
        } else {
          Some(BadRequest("Bad request."))
        }
      } getOrElse {
        BadRequest("Bad request.")
      }
    }
  }

  def signOut: Action[AnyContent] = Action {
    Redirect(routes.HomeController.index())
      .withNewSession
  }

  def redirectToDiscordSignIn: Action[AnyContent] = Action { implicit req =>
    val randomInt = random.nextInt()
    val newState = Integer.toString(randomInt, 36)

    discordStates.add(newState)
    Redirect(discordAuthService.oauthConsentUri(newState))
      .addingToSession("discord_state" -> newState)
  }

  def setDiscord: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    ru.getQueryString("error").map { error =>
      BadRequest(s"Got an error from Discord: $error\n\nPlease try again later.")
    } getOrElse {
      ru.getQueryString("state").flatMap { state =>
        if (discordStates.contains(state)) {
          discordStates.remove(state)
          ru.getQueryString("code").map { code =>
            val identity = discordAuthService.getIdentity(code)
            db.setDiscordSnowflake(ru, Some(identity.id))
            Redirect(routes.AccountController.viewAccount(ru._2.id))
              .removingFromSession("discord_state")(ru)
          }
        } else {
          Some(BadRequest("Bad request."))
        }
      } getOrElse {
        BadRequest("Bad request.")
      }
    }
  }

  def redirectToRedditRefreshGrant(mobile: Boolean): Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Redirect(redditAuthService.oauthConsentUriForRefreshToken(createNewState, mobile))
  }

  def redirectToGoogleAuth: Action[AnyContent] = Action { implicit req =>
    val randomInt = random.nextInt()
    val newState = Integer.toString(randomInt, 36)
    googleStates.add(newState)

    Redirect(googleAuthService.oauthUri(newState, "openid"))
  }

  def handleGoogleAuthResponse: Action[AnyContent] = Action { implicit req =>
    req.getQueryString("error").map { error =>
      BadRequest(s"Got an error response from google: $error\n\nPlease return to the homepage and try again.")
    } getOrElse {
      // Normal auth
      req.getQueryString("code").flatMap { code =>
        req.getQueryString("state").map { state =>
          val identity = googleAuthService.getIdentity(code)

          // Set gauth id if needed
          user.map { user =>
            db.setGoogleId(user, Some(identity))
            Redirect(routes.AccountController.viewAccount(user.id))
          } getOrElse {
            // otherwise sign in
            db.getUserByGoogle(identity).map { user =>
              Redirect(routes.AccountController.viewAccount(user.id))
                .withSession("uid" -> user.id.toString)
            } getOrElse {
              BadRequest(Html("I don't recognize you yet. Please <a href=\"" + routes.AuthController.redirectToSignIn(routes.AccountController.myAccount().url, true) + "\">sign in with Reddit first</a>, then link your Google account under My Account to use this feature."))
            }
          }
        }
      } getOrElse {
        BadRequest("I didn't get all the info needed from Google.\n\nPlease return to the homepage and try again.")
      }
    }
  }

}
