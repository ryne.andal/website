package services

import javax.inject.{Inject, Singleton}
import model._

// There's a lot of stuff in this file that could *probably* be refactored and made more pretty,
// although I probably won't revisit it since it's reasonably straightforward for now, albeit verbose
@Singleton
class MLRCalculatorService @Inject()(implicit db: FBDatabase) {

  private val INFIELD_IN_RANGE: CalcRange = CalcRange(0, 0, 0, 18, 0, 0, 0, 0, -9, -9)

  def calculateDiff(swing: Int, pitch: Int): Int = {
    val max = Math.max(swing, pitch)
    val min = Math.min(swing, pitch)
    Math.min(max - min, 1000 - max + min)
  }

  def calculateRanges(batter: PlayerWithTypes, pitcher: PlayerWithTypes, park: Park, infieldIn: Boolean = false): CalcRange = {
    // Add linear ranges
    val pitchingType = pitcher.pitchingType.getOrElse(db.getPitchingTypeByShortcode("POS").get)
    var combinedRanges = batter.battingType + pitchingType
    if (pitcher.pitchingBonus.isDefined && batter.player.righthanded == pitcher.player.righthanded)
      combinedRanges += pitcher.pitchingBonus.get

    // Now do the whole park calculation bit
    val parkChanges = CalcRange(
      Math.round(park.factorHr * combinedRanges.rangeHR).toInt - combinedRanges.rangeHR,
      Math.round(park.factor3b * combinedRanges.range3B).toInt - combinedRanges.range3B,
      Math.round(park.factor2b * combinedRanges.range2B).toInt - combinedRanges.range2B,
      Math.round(park.factor1b * combinedRanges.range1B).toInt - combinedRanges.range1B,
      Math.round(park.factorBb * combinedRanges.rangeBB).toInt - combinedRanges.rangeBB,
      0, 0, 0, 0, 0
    )
    var parkChangesSum = parkChanges.rangeHR + parkChanges.range3B + parkChanges.range2B + parkChanges.range1B + parkChanges.rangeBB
    val direction = if (parkChangesSum < 0) 1 else -1

    var index = -1
    while (parkChangesSum != 0) {
      index += 1
      index %= 5
      val currentRange = index match {
        case 0 => combinedRanges.rangeFO + parkChanges.rangeFO
        case 1 => combinedRanges.rangeK + parkChanges.rangeK
        case 2 => combinedRanges.rangePO + parkChanges.rangePO
        case 3 => combinedRanges.rangeRGO + parkChanges.rangeRGO
        case 4 => combinedRanges.rangeLGO + parkChanges.rangeLGO
      }
      if (currentRange > 0)
        index match {
          case 0 => parkChanges.rangeFO += direction
          case 1 => parkChanges.rangeK += direction
          case 2 => parkChanges.rangePO += direction
          case 3 => parkChanges.rangeRGO += direction
          case 4 => parkChanges.rangeLGO += direction
        }
      parkChangesSum += direction
    }

    if (infieldIn)
      combinedRanges + parkChanges + INFIELD_IN_RANGE
    else
      combinedRanges + parkChanges
  }

  def calculateSwing(batter: PlayerWithTypes, swing: Int, pitcher: PlayerWithTypes, pitch: Int, park: Park, infieldIn: Boolean = false): SwingResult = {
    val diff = calculateDiff(swing, pitch)
    val ranges = calculateRanges(batter, pitcher, park, infieldIn).absolute

    if (diff < ranges.rangeHR)
      SwingResult(Some(diff), "HR", Some(0), Some(ranges.rangeHR - 1))
    else if (diff < ranges.range3B)
      SwingResult(Some(diff), "3B", Some(ranges.rangeHR), Some(ranges.range3B - 1))
    else if (diff < ranges.range2B)
      SwingResult(Some(diff), "2B", Some(ranges.range3B), Some(ranges.range2B - 1))
    else if (diff < ranges.range1B)
      SwingResult(Some(diff), "1B", Some(ranges.range2B), Some(ranges.range1B - 1))
    else if (diff < ranges.rangeBB)
      SwingResult(Some(diff), "BB", Some(ranges.range1B), Some(ranges.rangeBB - 1))
    else if (diff < ranges.rangeFO)
      SwingResult(Some(diff), "FO", Some(ranges.rangeBB), Some(ranges.rangeFO - 1))
    else if (diff < ranges.rangeK)
      SwingResult(Some(diff), "K", Some(ranges.rangeFO), Some(ranges.rangeK - 1))
    else if (diff < ranges.rangePO)
      SwingResult(Some(diff), "PO", Some(ranges.rangeK), Some(ranges.rangePO - 1))
    else if (diff < ranges.rangeRGO)
      SwingResult(Some(diff), "RGO", Some(ranges.rangePO), Some(ranges.rangeRGO - 1))
    else
      SwingResult(Some(diff), "LGO", Some(ranges.rangeRGO), Some(500))
  }

  def handleWalk(state: GameState): (Option[Int], Option[Int], Seq[Option[Int]]) = {
    // Out format is new (R2, R3, scorer)
    if (state.r1.isDefined) {
      if (state.r2.isDefined) {
        (state.r1, state.r2, Seq(state.r3))
      } else {
        (state.r1, state.r3, Seq(None))
      }
    } else {
      (state.r2, state.r3, Seq(None))
    }
  }

  def processPlayChanges(currentState: GameState, playType: Int, pitcher: Int, pitch: Option[Int], batter: Player, swing: Option[Int], result: SwingResult, newOutsT: Int, onFirst: Option[Int], onSecond: Option[Int], onThird: Option[Int], scorersT: Seq[Option[Int]]): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    // Update the outs counter and rollover inning if needed
    val resOuts = if (currentState.outs + newOutsT >= 3) 0 else currentState.outs + newOutsT
    val resInning = if (currentState.outs + newOutsT >= 3) currentState.inning + 1 else currentState.inning

    var resAwayScore = currentState.scoreAway
    var resHomeScore = currentState.scoreHome

    // In case they need to cleared
    var (r1, r2, r3) = (onFirst, onSecond, onThird)
    var scorers = scorersT

    // Updates scores if we didn't hit 3 out there
    val isAwayBatting = currentState.inning % 2 == 1
    if (resInning == currentState.inning) {
      if (isAwayBatting)
        resAwayScore += scorers.count(_.isDefined)
      else
        resHomeScore += scorers.count(_.isDefined)
    } else {
      // If inning flipped, remove baserunners
      r1 = None
      r2 = None
      r3 = None
    }

    // Calculate runner responsibilities
    val responsibilities: Map[Option[Int], Option[Int]] = Map(
      currentState.r1 -> currentState.r1Responsibility,
      currentState.r2 -> currentState.r2Responsibility,
      currentState.r3 -> currentState.r3Responsibility,
      Some(batter.id) -> Some(pitcher)
    )
    val r1R = responsibilities.get(r1).flatten
    val r2R = responsibilities.get(r2).flatten
    val r3R = responsibilities.get(r3).flatten

    // And scorer responsibilities
    var scorerResponsibilities = scorers.map(s => s -> responsibilities.get(s).flatten)

    // Increment the correct lineup batter position
    val resAwayBPos = if (isAwayBatting && playType != PLAY_TYPE_STEAL && playType != PLAY_TYPE_MULTI_STEAL) (currentState.awayBattingPosition % 9) + 1 else currentState.awayBattingPosition
    val resHomeBPos = if (!isAwayBatting && playType != PLAY_TYPE_STEAL && playType != PLAY_TYPE_MULTI_STEAL) (currentState.homeBattingPosition % 9) + 1 else currentState.homeBattingPosition

    // Now remove empty baserunner values and generate the scorer string
    scorers = scorers.filter(_.isDefined && resInning == currentState.inning)
    scorerResponsibilities = scorerResponsibilities.filter(_._1.isDefined && resInning == currentState.inning)
    val liftedScorers = scorerResponsibilities.lift
    val scorersStr = if (scorers.nonEmpty) Some(scorers.map(_.get).mkString(",")) else None

    val scoringPlayDesc = generateScoringPlayDescription(result, batter, scorers.map(_.get))

    val newOuts = Math.min(currentState.outs + newOutsT, 3) - currentState.outs
    // Return the new game state
    (
      PartialGameAction(playType, pitcher, pitch, batter.id, swing, result.diff, Some(result.result), scorers.size, scorersStr, newOuts),
      new GameState(0, resAwayBPos, resHomeBPos, r1, r2, r3, r1R, r2R, r3R, resOuts, resInning, resAwayScore, resHomeScore),
      if (scorers.nonEmpty) Some(new ScoringPlay(0, 0, liftedScorers(0).flatMap(_._1), liftedScorers(0).flatMap(_._2), liftedScorers(1).flatMap(_._1), liftedScorers(1).flatMap(_._2), liftedScorers(2).flatMap(_._1), liftedScorers(2).flatMap(_._2), liftedScorers(3).flatMap(_._1), liftedScorers(3).flatMap(_._2), scoringPlayDesc)) else None,
      result.resultMin,
      result.resultMax
    )
  }

  def generateScoringPlayDescription(result: SwingResult, batter: Player, scorerIds: Seq[Int]): String = {
    val scorers = scorerIds.map(db.getPlayerById(_).get)
    val scorerList = s"${scorers.map(_.lastName).mkString(", ")} score${if (scorers.size == 1) "s" else ""}"
    s"${batter.lastName} " +
      (result.result match {
      case "HR" => s"homers, $scorerList"
      case "3B" => s"triples, $scorerList"
      case "2B" => s"doubles, $scorerList"
      case "1B" => s"singles, $scorerList"
      case "BB" => s"walks, $scorerList"
      case "FO" => s"sac flies, $scorerList"
      case "LGO" | "RGO" => s"grounds out, $scorerList"
      case "Steal Home" => "steals home"
      case _ => "does something"
    })
  }

  def handleSwing(currentState: GameState, pitcherWithTypes: PlayerWithTypes, pitch: Int, batterWithTypes: PlayerWithTypes, swing: Int, park: Park, infieldIn: Boolean): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    // Determine swing result
    val result = calculateSwing(batterWithTypes, swing, pitcherWithTypes, pitch, park, infieldIn)

    // Wrapped types for the option placements
    val pitcher = Some(pitcherWithTypes.player.id)
    val batter = Some(batterWithTypes.player.id)
    val r1 = currentState.r1
    val r2 = currentState.r2
    val r3 = currentState.r3

    val runners = Seq(currentState.r1, currentState.r2, currentState.r3)
    var newOuts = 0
    var scorers: Seq[Option[Int]] = Seq()
    var onFirst: Option[Int] = None
    var onSecond: Option[Int] = None
    var onThird: Option[Int] = None

    if (result.result == "HR") {
      scorers = runners :+ batter
    } else if (result.result == "3B") {
      onThird = batter
      scorers = runners
    } else if (result.result == "2B") {
      onSecond = batter
      if (currentState.outs == 2) {
        scorers = runners
      } else {
        scorers = Seq(r2, r3)
        onThird = r1
      }
    } else if (result.result == "1B") {
      onFirst = batter
      if (currentState.outs == 2) {
        scorers = Seq(r2, r3)
        onThird = r1
      } else {
        scorers = Seq(r3)
        onThird = r2
        onSecond = r1
      }
    } else if (result.result == "BB") {
      val t = handleWalk(currentState)
      onFirst = batter
      onSecond = t._1
      onThird = t._2
      scorers = t._3
    } else if (result.result == "FO") {
      newOuts = 1
      scorers = Seq(r3)
      onSecond = r2
      onFirst = r1
    } else if (result.result == "K" || result.result == "PO") {
      newOuts = 1
      onThird = r3
      onSecond = r2
      onFirst = r1
    } else if (result.result == "RGO" && !infieldIn) {
      newOuts = if (r1.isDefined) 2 else 1
      scorers = Seq(r3)
      onThird = r2
    } else if (result.result == "LGO" && !infieldIn) {
      newOuts = if (r1.isDefined) if (r2.isDefined && result.diff.get > 495) 3 else 2 else 1
      scorers = Seq(r3)
      onThird = if (r1.isDefined) r2 else None
      onSecond = if (r1.isEmpty) r2 else None
    } else if (infieldIn && (result.result == "RGO" || result.result == "LGO")) {
      newOuts = 1
      if (r1.isDefined && r2.isDefined) {
        onThird = r2
        onSecond = r1
        onFirst = batter
      } else {
        onThird = r3
        onSecond = if (r2.isDefined) r2 else r1
      }
    }

    processPlayChanges(currentState, if (infieldIn) PLAY_TYPE_INFIELD_IN else PLAY_TYPE_SWING, pitcher.get, Some(pitch), batterWithTypes.player, Some(swing), result, newOuts, onFirst, onSecond, onThird, scorers)
  }

  def handleAutoK(currentState: GameState, pitcher: PlayerWithTypes, batter: PlayerWithTypes): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    processPlayChanges(currentState, PLAY_TYPE_AUTO_K, pitcher.player.id, None, batter.player, None, SwingResult(None, "Auto K", None, None), 1, currentState.r1, currentState.r2, currentState.r3, Seq())
  }

  def handleAutoBB(currentState: GameState, pitcher: PlayerWithTypes, batter: PlayerWithTypes): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    val (onSecond, onThird, scorers) = handleWalk(currentState)
    processPlayChanges(currentState, PLAY_TYPE_AUTO_BB, pitcher.player.id, None, batter.player, None, SwingResult(None, "Auto BB", None, None), 0, Some(batter.player.id), onSecond, onThird, scorers)
  }

  def handleIBB(currentState: GameState, pitcher: PlayerWithTypes, batter: PlayerWithTypes): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    val (onSecond, onThird, scorers) = handleWalk(currentState)
    processPlayChanges(currentState, PLAY_TYPE_IBB, pitcher.player.id, None, batter.player, None, SwingResult(None, "IBB", None, None), 0, Some(batter.player.id), onSecond, onThird, scorers)
  }

  def handleBunt(currentState: GameState, pitcher: Player, pitch: Int, batter: Player, swing: Int): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    val diff = calculateDiff(swing, pitch)

    // Determine output
    val (result, minRange, maxRange, scorers, newOuts, onFirst, onSecond, onThird): (String, Int, Int, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = {
      if (diff <= 50) {
        ("Bunt 1B", 0, 50, Seq(currentState.r3), 0, Some(batter.id), currentState.r1, currentState.r2)
      } else if (diff <= 375) {
        if (currentState.r3.isDefined) {
          if (currentState.r2.isDefined) {
            ("Bunt Sac", 51, 375, Seq(), 1, currentState.r1, currentState.r2, currentState.r3)
          } else {
            ("Bunt Sac", 51, 375, Seq(), 1, None, currentState.r1, currentState.r3)
          }
        } else {
          ("Bunt Sac", 51, 375, Seq(), 1, None, currentState.r1, currentState.r2)
        }
      } else if (diff <= 475) {
        ("Bunt K", 376, 475, Seq(), 1, currentState.r1, currentState.r2, currentState.r3)
      } else {
        // 476+ diff
        // https://docs.google.com/document/d/1XA3BTY51PfxdAMjv4vktA8KZY2zDkuSsLrNnF6ZCDB4/edit
        if (currentState.r1.isDefined) {
          if (currentState.r2.isDefined) {
            (if (currentState.outs < 2) "Bunt DP" else "Bunt Sac", 476, 500, Seq(), 2, None, currentState.r1.filter(_ => currentState.r3.isDefined), currentState.r2)
          } else {
            if (currentState.r3.isDefined) { // R13 corners
              if (currentState.outs == 1)
                ("Bunt DP", 476, 500, Seq(), 2, None, None, None)
              else
                ("Bunt GO", 476, 500, Seq(), 1, Some(batter.id), None, currentState.r3)
            } else { // R1
              (if (currentState.outs < 2) "Bunt DP" else "Bunt Sac", 476, 500, Seq(), 2, None, None, None)
            }
          }
        } else { // first empty, R23 never move
          ("Bunt GO", 476, 500, Seq(), 1, None, currentState.r2, currentState.r3)
        }
      }
    }

    val res = SwingResult(Some(diff), result, Some(minRange), Some(maxRange))

    processPlayChanges(currentState, PLAY_TYPE_BUNT, pitcher.id, Some(pitch), batter, Some(swing), res, newOuts, onFirst, onSecond, onThird, scorers)
  }

  def handleSteal(currentState: GameState, pitcher: Player, pitch: Int, stealer: Player, steal: Int): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    val diff = calculateDiff(steal, pitch)

    val (result, minRange, maxRange, scorers, newOuts, onFirst, onSecond, onThird): (String, Int, Int, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = {
      if (currentState.r1.contains(stealer.id)) { // Stealing second
        if (diff <= 300)
          ("Steal 2B", 0, 300, Seq(), 0, None, currentState.r1, currentState.r3)
        else
          ("CS 2B", 301, 500, Seq(), 1, None, None, currentState.r3)
      } else if (currentState.r2.contains(stealer.id)) { // Stealing third
        if (diff <= 150)
          ("Steal 3B", 0, 150, Seq(), 0, currentState.r1, None, currentState.r2)
        else
          ("CS 3B", 151, 500, Seq(), 1, currentState.r1, None, None)
      } else { // Stealing home
        if (diff <= 25)
          ("Steal Home", 0, 25, Seq(currentState.r3), 0, currentState.r1, currentState.r2, None)
        else
          ("CS Home", 26, 500, Seq(), 1, currentState.r1, currentState.r2, None)
      }
    }

    val res = SwingResult(Some(diff), result, Some(minRange), Some(maxRange))
    processPlayChanges(currentState, PLAY_TYPE_STEAL, pitcher.id, Some(pitch), stealer, Some(steal), res, newOuts, onFirst, onSecond, onThird, scorers)
  }

  def handleMultiSteal(currentState: GameState, pitcher: Player, pitch: Int, stealer: Player, steal: Int): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = {
    val diff = calculateDiff(steal, pitch)

    val (result, minRange, maxRange, scorers, newOuts, onFirst, onSecond, onThird): (String, Int, Int, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = {
      if (currentState.r2.contains(stealer.id)) { // Stealing second and third
        if (diff <= 150)
          ("MSteal 3B", 0, 150, Seq(), 0, None, currentState.r1, currentState.r2)
        else
          ("CMS 3B", 151, 500, Seq(), 1, None, currentState.r1, None)
      } else { // Stealing home
        if (diff <= 25)
          ("MSteal Home", 0, 25, Seq(currentState.r3), 0, None, currentState.r1, currentState.r2)
        else
          ("CMS Home", 26, 500, Seq(), 1, None, currentState.r1, currentState.r2)
      }
    }

    val res = SwingResult(Some(diff), result, Some(minRange), Some(maxRange))
    processPlayChanges(currentState, PLAY_TYPE_MULTI_STEAL, pitcher.id, Some(pitch), stealer, Some(steal), res, newOuts, onFirst, onSecond, onThird, scorers)
  }

}
