package services

import controllers.routes
import javax.inject.{Inject, Singleton}
import model._
import play.api.mvc.{AnyContent, Request}

@Singleton
class RedditFormattingService @Inject()() {

  def generateBoxScore(game: GameWithGameState, allPlays: Seq[GameActionWithStatesAndPlayers], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], scoringPlays: Seq[ScoringPlayWithActionAndStates], gameAwards: Option[GameAwardsWithPlayers])(implicit req: Request[AnyContent]): String = {
    val awayScore = game.state.scoreAway
    val homeScore = game.state.scoreHome

    // Generate the harder parts
    val line = generateLineScore(allPlays, game.awayTeam.tag, game.homeTeam.tag, game.game.completed)
    val box = generateLineupBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name)
    val pitchers = generatePitcherBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name, scoringPlays.map(_.scoringPlay))
    val scoringPlaysBox = generateScoringPlaysBox(scoringPlays)
    val awards = gameAwards.map(generateAwardsSection)

    // Put it all together
   s"## ${game.awayTeam.tag} $awayScore - $homeScore ${game.homeTeam.tag}\n\n[View on Redditball](${routes.GameController.viewGame(game.game.id).absoluteURL()})\n\n${if (awards.isDefined) s"## AWARDS  \n${awards.get}\n\n" else ""}## LINE\n$line\n\n## BOX\n$box\n\n## PITCHERS\n$pitchers\n\n## SCORING PLAYS\n$scoringPlaysBox"
  }

  def generateAwardsSection(awards: GameAwardsWithPlayers): String = {
    var ret = s"**WP**: ${awards.winningPitcher.fullName}  \n" +
    s"**LP**: ${awards.losingPitcher.fullName}  \n" +
    s"**PotG**: ${awards.playerOfTheGame.fullName}  \n"
    awards.save.foreach { save =>
      ret += s"**SV**: ${save.fullName}"
    }
    ret
  }

  def generateLineScore(allPlays: Seq[GameActionWithStatesAndPlayers], awayTeam: String, homeTeam: String, complete: Boolean): String = {

    // Always present
    var line1 = "||"
    var line2 = "|:--|"
    var line3 = s"|**$awayTeam**|"
    var line4 = s"|**$homeTeam**|"

    // Track max inning played
    val maxInning = if (allPlays.isEmpty) 0 else allPlays.map(_.beforeState.inning).max

    // Loop all innings played, but at least 6
    for (i <- 1 to Math.max(maxInning, 12)) {
      // Determine if the current inning being added is the active inning
      val italics = i == maxInning && !complete
      // Add away score if odd, home score if even
      if (i % 2 == 1) {
        line1 += s"${(i + 1) / 2}|"
        line2 += ":--|"

        // Only add score content if we have reached that inning
        if (i <= maxInning)
          line3 += s"${if (italics) "*" else ""}${allPlays.filter(_.beforeState.inning == i).map(_.gameAction.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line3 += "|"
      } else {
        if (i <= maxInning)
          line4 += s"${if (italics) "*" else ""}${allPlays.filter(_.beforeState.inning == i).map(_.gameAction.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line4 += "|"
      }
    }

    line1 += "R|H|"
    line2 += ":--|:--|"
    line3 += s"${allPlays.headOption.map(_.afterState.scoreAway).getOrElse(0)}|${allPlays.count(p => p.beforeState.inning % 2 == 1 && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|"
    line4 += s"${allPlays.headOption.map(_.afterState.scoreHome).getOrElse(0)}|${allPlays.count(p => p.beforeState.inning % 2 == 0 && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|"

    // Return lines combined
    s"\n$line1\n$line2\n$line3\n$line4"
  }

  def generateLineupBox(allPlays: Seq[GameActionWithStatesAndPlayers], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String): String = {
    val header = s"|\\#|$awayTeam|Pos|AB|R|H|RBI|BB|K|BA|\\#|$homeTeam|Pos|AB|R|H|RBI|BB|K|BA|\n" +
      "|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var lineupBox = ""
    for (i <- 1 to 9) {
      // Figure out how many rows we need for this batting position
      val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

      // Now start putting in the players, or blank lines if none
      for (j <- 0 until rows) {
        val awayPlayer = aways.flatMap(_.lift(j))
        lineupBox += awayPlayer.map(entry => {
          s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.gameAction.batter.contains(entry.player.player.id) && (p.gameAction.playType != PLAY_TYPE_STEAL && p.gameAction.playType != PLAY_TYPE_MULTI_STEAL && p.gameAction.playType != PLAY_TYPE_IBB && p.gameAction.playType != PLAY_TYPE_AUTO_BB && !p.gameAction.result.contains("BB") && !p.gameAction.result.contains("Bunt Sac") && !(p.gameAction.result.contains("FO") && p.gameAction.runsScored == 1)))}|" +
            s"${allPlays.count(_.gameAction.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.player.id)))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.id == entry.player.player.id && p.gameAction.outsTracked < 2).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType == PLAY_TYPE_IBB || p.gameAction.playType == PLAY_TYPE_AUTO_BB || p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.map(_.ba).getOrElse(0f))}|"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|")
        val homePlayer = homes.flatMap(_.lift(j))
        lineupBox += homePlayer.map(entry => {
          s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.gameAction.batter.contains(entry.player.player.id) && (p.gameAction.playType != PLAY_TYPE_STEAL && p.gameAction.playType != PLAY_TYPE_MULTI_STEAL && p.gameAction.playType != PLAY_TYPE_IBB && p.gameAction.playType != PLAY_TYPE_AUTO_BB && !p.gameAction.result.contains("BB") && !p.gameAction.result.contains("Bunt Sac") && !(p.gameAction.result.contains("FO") && p.gameAction.runsScored == 1)))}|" +
            s"${allPlays.count(_.gameAction.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.player.id)))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.id == entry.player.player.id && p.gameAction.outsTracked < 2).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType == PLAY_TYPE_IBB || p.gameAction.playType == PLAY_TYPE_AUTO_BB || p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.map(_.ba).getOrElse(0f))}|\n"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|\n")
      }
    }

    // Return the combined string
    s"$header\n$lineupBox"
  }

  def generatePitcherBox(allPlays: Seq[GameActionWithStatesAndPlayers], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String, scoringPlays: Seq[ScoringPlay]): String = {
    val header = s"|$awayTeam|IP|H|ER|BB|K|ERA|$homeTeam|IP|H|ER|BB|K|ERA|\n" +
      s"|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var pitcherBox = ""

    // Figure out how many rows we need for this table
    val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

    // Now start putting in the players, or blank lines if none
    for (j <- 0 until rows) {
      val awayPlayer = aways.flatMap(_.lift(j))
      pitcherBox += awayPlayer.map(entry => {
        s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
          s"${scoringPlays.count(_.scorer1Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer2Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer3Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer4Responsibility.contains(entry.player.player.id))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("BB") || p.gameAction.result.contains("Auto BB")))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.map(_.era).getOrElse(0f)).replace("Infinity", "Inf.")}|"
      }).getOrElse("|--|--|--|--|--|--|--|")
      val homePlayer = homes.flatMap(_.lift(j))
      pitcherBox += homePlayer.map(entry => {
        s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
          s"${scoringPlays.count(_.scorer1Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer2Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer3Responsibility.contains(entry.player.player.id)) + scoringPlays.count(_.scorer4Responsibility.contains(entry.player.player.id))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("BB") || p.gameAction.result.contains("Auto BB")))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.map(_.era).getOrElse(0f)).replace("Infinity", "Inf.")}|\n"
      }).getOrElse("--|--|--|--|--|--|--|\n")
    }

    // Combine and return
    s"$header\n$pitcherBox"
  }

  def generateScoringPlaysBox(scoringPlays: Seq[ScoringPlayWithActionAndStates]): String = {
    val header = "|Inning|Play|Score|\n|:--|:--|:--|"

    val plays = scoringPlays
      .sortBy(_.gameAction.id)
      .map(sp => s"|${sp.beforeState.inningStr}|${sp.scoringPlay.description}|${sp.afterState.scoreAway} - ${sp.afterState.scoreHome}|")
      .mkString("\n")

    s"$header\n$plays"
  }

  def generateBatterPing(game: GameWithGameState, batter: LineupEntryWithPlayer, pitcher: LineupEntryWithPlayer, allPlays: Seq[GameActionWithStatesAndPlayers], batterPingFormat: String): String = {
    var hits = 0
    var abs = 0
    var plays = Seq[String]()
    allPlays.reverse.foreach { play =>
      if (play.gameAction.batter.contains(batter.player.player.id)) {
        // Count hits/ABs
        if (play.gameAction.result.contains("HR") || play.gameAction.result.contains("3B") || play.gameAction.result.contains("2B") || play.gameAction.result.contains("1B")) {
          hits += 1
          abs += 1
        } else if (play.gameAction.result.contains("FO") && play.gameAction.runsScored == 0) {
          abs += 1
        } else if (play.gameAction.result.contains("K") || play.gameAction.result.contains("Bunt K") || play.gameAction.result.contains("Auto K") || play.gameAction.result.contains("PO") || play.gameAction.result.contains("RGO") || play.gameAction.result.contains("LGO")) {
          abs += 1
        }

        // Make list of plays
        val i = play.beforeState.inning
        val inning = if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}"
        plays :+= s"${play.gameAction.result.get} in $inning"
      }
    }

    val awayTag = game.awayTeam.tag
    val homeTag = game.homeTeam.tag
    val awayScore = game.state.scoreAway.toString
    val homeScore = game.state.scoreHome.toString
    val basesDiamonds = s" ${if (game.state.r3.isDefined) "\u25C6" else "\u25C7"} ^${if (game.state.r2.isDefined) "\u25C6" else "\u25C7"} ${if (game.state.r1.isDefined) "\u25C6" else "\u25C7"}"
    val inning = game.inning
    val outs = game.state.outs.toString
    val batterTeamTag = if (game.awayBatting) game.awayTeam.tag else game.homeTeam.tag
    val batterPosition = batter.lineupEntry.position
    val batterPing = s"[${batter.player.player.fullName}](${batter.player.user.redditName})"
    val batterRecord = s"$hits-$abs${if (plays.nonEmpty) s": ${plays.mkString(", ")}" else ""}"

    val pitcherName = pitcher.player.player.name
    val pitchingChange =
      if (allPlays.exists(_.pitcher.id == pitcher.player.player.id))
        ""
      else
        s"**New pitcher: ${pitcher.player.player.name}**"

    batterPingFormat
      .replace("%awayTag%", awayTag)
      .replace("%homeTag%", homeTag)
      .replace("%awayScore%", awayScore)
      .replace("%homeScore%", homeScore)
      .replace("%basesDiamonds%", basesDiamonds)
      .replace("%inning%", inning)
      .replace("%outs%", outs)
      .replace("%batterTeamTag%", batterTeamTag)
      .replace("%batterPosition%", batterPosition)
      .replace("%batterPing%", batterPing)
      .replace("%batterRecord%", batterRecord)
      .replace("%pitchingChange%", pitchingChange)
      .replace("%pitcherName%", pitcherName)
      .replaceAll("\n{3,}", "\n\n")
  }

}
