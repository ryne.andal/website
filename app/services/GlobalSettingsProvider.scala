package services

import javax.inject.{Inject, Singleton}
import model._

@Singleton
class GlobalSettingsProvider @Inject()(db: FBDatabase) {

  def INDEX_MESSAGE: Option[String] = getSetting("index.message")
  def AB_PINGS_CHANNEL: Option[String] = getSetting("discord.channel.ping")
  def UMPTY_COMMAND_CHANNEL: Option[String] = getSetting("discord.channel.command")
  def DISCORD_WEBHOOK: Option[String] = getSetting("discord.webhook")
  def DISCORD_FREE_AGENT_ROLE: Option[String] = getSetting("discord.farole")
  def SUBREDDIT: Option[String] = getSetting("reddit.subreddit")

  private def getSetting(name: String): Option[String] = {
    db.getGlobalSetting(name).map(_.value)
  }

  private def setSetting(name: String, value: String): Unit = {
    db.setGlobalSetting(new GlobalSetting(name, value))
  }

}
