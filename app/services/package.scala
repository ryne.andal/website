import play.api.libs.json.{JsValue, Json, Writes}

package object services {

  case class DiscordUserInfo(id: String, username: String, discriminator: String)

  case class RedditUserInfo(id: String, name: String, isEmailVerified: Boolean, isSuspended: Boolean, linkKarma: Int, commentKarma: Int, creationTimestamp: Long)

  case class RedditRequest(endpoint: String, body: Map[String, Seq[String]])

  case class RedditResponse(status: Int, body: JsValue)

  def RedditSubmissionRequest(kind: String, sr: String, title: String, text: String): RedditRequest =
    RedditRequest(
      endpoint = "submit",
      body = Map(
        "kind" -> Seq(kind),
        "sr" -> Seq(sr),
        "title" -> Seq(title),
        "text" -> Seq(text)
      )
    )

  def RedditEditRequest(thing: String, text: String): RedditRequest =
    RedditRequest(
      endpoint = "editusertext",
      body = Map(
        "thing_id" -> Seq(s"t3_$thing"),
        "text" -> Seq(text),
      )
    )

  def RedditCommentRequest(thing: String, text: String): RedditRequest =
    RedditRequest(
      endpoint = "comment",
      body = Map(
        "thing_id" -> Seq(s"t3_$thing"),
        "text" -> Seq(text)
      )
    )
}
