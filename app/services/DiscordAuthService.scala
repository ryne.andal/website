package services

import java.net.URLEncoder
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import play.Environment
import play.api.Configuration
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.ws.{WSAuthScheme, WSClient}
import play.api.mvc.{AnyContent, Request}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

@Singleton
class DiscordAuthService @Inject()(config: Configuration, env: Environment, ws: WSClient)(implicit ec: ExecutionContext) {

  private implicit val DiscordUserInfoReads: Reads[DiscordUserInfo] = Json.reads[DiscordUserInfo]

  private lazy val clientId = config.get[String]("discord.clientId")
  private lazy val clientSecret = config.get[String]("discord.clientSecret")

  def encodeRedirectUri(implicit req: Request[AnyContent]): String = URLEncoder.encode((if (env.isDev) "http://" else "https://") + req.host + "/auth/discord", "UTF-8")

  def oauthConsentUri(state: String)(implicit req: Request[AnyContent]): String = {
    s"https://discordapp.com/api/oauth2/authorize?client_id=$clientId&response_type=code&state=$state&redirect_uri=$encodeRedirectUri&scope=identify"
  }

  def getIdentity(authCode: String)(implicit req: Request[AnyContent]): DiscordUserInfo = {
    val accessTokenResponse = Await.result(
      ws.url("https://discordapp.com/api/oauth2/token")
        .withAuth(clientId, clientSecret, WSAuthScheme.BASIC)
        .withMethod("POST")
        .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded")
        .post(s"grant_type=authorization_code&code=$authCode&redirect_uri=$encodeRedirectUri&scope=identify"),
      Duration(5, TimeUnit.SECONDS)
    )
    val accessToken = (accessTokenResponse.json \ "access_token").as[String]

    val myInfoResponse = Await.result(
      ws.url("https://discordapp.com/api/v6/users/@me")
        .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Authorization" -> s"Bearer $accessToken")
        .get(),
      Duration(5, TimeUnit.SECONDS)
    )
    myInfoResponse.json.as[DiscordUserInfo]
  }

}
