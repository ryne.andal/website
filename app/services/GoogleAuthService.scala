package services

import java.net.URLEncoder
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import play.Environment
import play.api.Configuration
import play.api.libs.ws.WSClient
import play.api.mvc.{AnyContent, Request}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

@Singleton
class GoogleAuthService @Inject()(config: Configuration, env: Environment, ws: WSClient)(implicit ec: ExecutionContext) {

  private lazy val CLIENT_ID = config.get[String]("google.clientId")
  private lazy val CLIENT_SECRET = config.get[String]("google.clientSecret")

  def encodeRedirectUri(implicit req: Request[AnyContent]): String = URLEncoder.encode((if (env.isDev) "http://" else "https://") + req.host + "/auth/google", "UTF-8")

  def oauthUri(state: String, scopes: String)(implicit req: Request[AnyContent]): String = {
    s"https://accounts.google.com/o/oauth2/v2/auth?client_id=$CLIENT_ID&response_type=code&state=$state&redirect_uri=$encodeRedirectUri&access_type=online&scope=$scopes"
  }

  def getIdentity(code: String)(implicit req: Request[AnyContent]): String = {
    val accessToken = (Await.result(
      ws
        .url("https://www.googleapis.com/oauth2/v4/token")
        .addHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded")
        .post(s"code=$code&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&redirect_uri=$encodeRedirectUri&grant_type=authorization_code"),
      Duration(10, TimeUnit.SECONDS)
    ).json \ "access_token").as[String]

    val uid = (Await.result(
      ws
        .url("https://www.googleapis.com/oauth2/v2/userinfo")
        .addHttpHeaders("Authorization" -> s"Bearer $accessToken")
        .get(),
      Duration(10, TimeUnit.SECONDS)
    ).json \ "id").as[String]

    ws.url(s"https://accounts.google.com/o/oauth2/revoke?token=$accessToken")
      .get()

    uid
  }

}
