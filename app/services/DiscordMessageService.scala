package services

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.DiscordTypes._
import model.{Game, PlayerWithUser, User}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class DiscordMessageService @Inject()(settingsProvider: GlobalSettingsProvider, config: Configuration, ws: WSClient) {

  private def webhookRequest(discordMessageRequest: DiscordMessageRequest): WSResponse =
    Await.result(
      ws.url(settingsProvider.DISCORD_WEBHOOK.get)
        .post(Json.toJson(discordMessageRequest)),
      Duration(5, TimeUnit.SECONDS)
    )

  def sendABAlert(player: PlayerWithUser, game: Game, umpires: Seq[User]): Unit = webhookRequest(
    UmptyABPingRequest(player, game, umpires)
  )

  def roleUser(user: String, role: String, remove: Boolean): Unit = webhookRequest(
    UmptyRoleRequest(user, role, remove)
  )

}
