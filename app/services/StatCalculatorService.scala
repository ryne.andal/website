package services

import model._

object StatCalculatorService {

  // region batting

  def calculateHits(stats: BattingStatSet): Int = stats.totalHr + stats.total3b + stats.total2b + stats.total1b

  def calculateBA(stats: BattingStatSet): Float = if (stats.totalAbs == 0) 0 else calculateHits(stats).toFloat / stats.totalAbs

  def calculateOBP(stats: BattingStatSet): Float = if (stats.totalPas == 0) 0 else (calculateHits(stats) + stats.totalBb).toFloat / stats.totalPas

  def calculateSLG(stats: BattingStatSet): Float = if (stats.totalAbs == 0) 0 else (4 * stats.totalHr + 3 * stats.total3b + 2 * stats.total2b + 1 * stats.total1b).toFloat / stats.totalAbs

  def calculateOPS(stats: BattingStatSet): Float = calculateOBP(stats) + calculateSLG(stats)

  // endregion

  // region pitching

  def calculateIP(stats: PitchingStatSet): Float = (stats.totalOuts / 3) + 0.1f * (stats.totalOuts % 3)

  def calculateERA(stats: PitchingStatSet): Float = if (stats.totalEr == 0) 0 else if (stats.totalOuts == 0) Float.PositiveInfinity else (stats.totalEr * 18).toFloat / stats.totalOuts.toFloat

  def calculateWHIP(stats: PitchingStatSet): Float = if (calculateIP(stats) == 0) 0 else (stats.totalHr + stats.total3b + stats.total2b + stats.total1b + stats.totalBb)*3 / stats.totalOuts.toFloat

  def calculateGO(stats: PitchingStatSet): Int = stats.totalLgo + stats.totalRgo

  // endregion

}
