package services

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model._
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class RedditPostService @Inject()(redditFormattingService: RedditFormattingService, ws: WSClient, settingsProvider: GlobalSettingsProvider) {

  private def authenticatedRequest(accessToken: String, request: RedditRequest): RedditResponse = {
    val result = Await.result(
      ws.url(s"https://oauth.reddit.com/api/${request.endpoint}")
        .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Authorization" -> s"Bearer $accessToken")
        .post(request.body),
      Duration(5, TimeUnit.SECONDS)
    )
    RedditResponse(result.status, result.json)
  }

  def createGameThread(accessToken: String, title: String, boxScoreText: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditSubmissionRequest(
        kind = "self",
        sr = settingsProvider.SUBREDDIT.getOrElse("fakebaseball"),
        title = title,
        text = boxScoreText,
      )
    )

  def editGameThread(accessToken: String, id36: String, newbody: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditEditRequest(
        thing = id36,
        text = newbody
      )
    )

  def postReplyToThread(accessToken: String, id36: String, commentBody: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditCommentRequest(
        thing = id36,
        text = commentBody
      )
    )

}
