package model

import java.util.Date

import play.api.libs.json.{Json, OWrites}

object DiscordTypes {

  case class DiscordRichEmbedFooter(text: String, icon_url: Option[String] = None, proxy_icon_url: Option[String] = None)
  implicit val DISCORD_RICH_EMBED_FOOTER_WRITE: OWrites[DiscordRichEmbedFooter] = Json.writes[DiscordRichEmbedFooter]
  case class DiscordRichEmbedImage(url: Option[String] = None, proxy_url: Option[String] = None, height: Option[Int] = None, width: Option[Int] = None)
  implicit val DISCORD_RICH_EMBED_IMAGE_WRITE: OWrites[DiscordRichEmbedImage] = Json.writes[DiscordRichEmbedImage]
  case class DiscordRichEmbedThumbnail(url: Option[String] = None, proxy_url: Option[String] = None, height: Option[Int] = None, width: Option[Int] = None)
  implicit val DISCORD_RICH_EMBED_THUMBNAIL_WRITE: OWrites[DiscordRichEmbedThumbnail] = Json.writes[DiscordRichEmbedThumbnail]
  case class DiscordRichEmbedVideo(url: Option[String] = None, height: Option[Int] = None, width: Option[Int] = None)
  implicit val DISCORD_RICH_EMBED_VIDEO_WRITE: OWrites[DiscordRichEmbedVideo] = Json.writes[DiscordRichEmbedVideo]
  case class DiscordRichEmbedProvider(name: Option[String] = None, url: Option[String] = None)
  implicit val DISCORD_RICH_EMBED_PROVIDER_WRITE: OWrites[DiscordRichEmbedProvider] = Json.writes[DiscordRichEmbedProvider]
  case class DiscordRichEmbedAuthor(name: Option[String] = None, url: Option[String] = None, icon_url: Option[String] = None, proxy_icon_url: Option[String] = None)
  implicit val DISCORD_RICH_EMBED_AUTHOR_WRITE: OWrites[DiscordRichEmbedAuthor] = Json.writes[DiscordRichEmbedAuthor]
  case class DiscordRichEmbedField(name: String, value: String, inline: Option[Boolean] = None)
  implicit val DISCORD_RICH_EMBED_FIELD_WRITE: OWrites[DiscordRichEmbedField] = Json.writes[DiscordRichEmbedField]
  case class DiscordRichEmbed(title: Option[String] = None, `type`: Option[String] = None, description: Option[String] = None, url: Option[String] = None, timestamp: Option[Date] = None, color: Option[Int], footer: Option[DiscordRichEmbedFooter] = None, image: Option[DiscordRichEmbedImage] = None, thumbnail: Option[DiscordRichEmbedThumbnail] = None, video: Option[DiscordRichEmbedVideo] = None, provider: Option[DiscordRichEmbedProvider] = None, author: Option[DiscordRichEmbedAuthor] = None, field: Option[Seq[DiscordRichEmbedField]] = None)
  implicit val DISCORD_RICH_EMBED_WRITE: OWrites[DiscordRichEmbed] = Json.writes[DiscordRichEmbed]
  case class DiscordMessageRequest(content: Option[String] = None, tts: Option[Boolean] = Some(false), embeds: Seq[DiscordRichEmbed] = Seq())
  implicit val DISCORD_MESSAGE_REQUEST: OWrites[DiscordMessageRequest] = Json.writes[DiscordMessageRequest]

  def UmptyCommandRequest(command: String, args: String*): DiscordMessageRequest = DiscordMessageRequest(content = Some(s"$command|${args.mkString("|")}"))

  def UmptyABPingRequest(player: PlayerWithUser, game: Game, umpires: Seq[User]): DiscordMessageRequest = UmptyCommandRequest("ABPING", player.user.discord.get, player.player.name, game.milr.toString, game.id36.get, umpires.map(_.discord.getOrElse("UNVERIFIED")).mkString(" "))
  def UmptyRoleRequest(user: String, role: String, remove: Boolean): DiscordMessageRequest = UmptyCommandRequest("ROLE", user, role, remove.toString)

}
