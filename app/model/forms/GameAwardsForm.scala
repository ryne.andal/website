package model.forms

import model.FBDatabase
import play.api.data.Forms._
import play.api.data._

case class GameAwardsForm(winningPitcher: Int, losingPitcher: Int, playerOfTheGame: Int, save: Option[Int])

object GameAwardsForm {

  def gameAwardsForm(implicit db: FBDatabase): Form[GameAwardsForm] = Form(
    mapping(
      "winningPitcher" -> number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined),
      "losingPitcher" -> number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined),
      "playerOfTheGame" -> number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined),
      "save" -> optional(number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined))
    )(GameAwardsForm.apply)(GameAwardsForm.unapply)
  )

}