package model.forms

import model.{FBDatabase, Player}
import play.api.data.Forms._
import play.api.data._

case class SetGmForm(season: Int, session: Int, oldGMReason: Option[String], newGM: Player)

object SetGmForm {

  def setGmForm(implicit db: FBDatabase) = Form(
    mapping(
      "season" -> number,
      "session" -> number,
      "oldGMReason" -> optional(nonEmptyText),
      "newGM" -> playerFormat
    )(SetGmForm.apply)(SetGmForm.unapply)
  )

}
