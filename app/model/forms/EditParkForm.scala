package model.forms

import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._

case class EditParkForm(name: String, factorHR: Double, factor3B: Double, factor2B: Double, factor1B: Double, factorBB: Double)

object EditParkForm {

  val editParkForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "factorHR" -> of(doubleFormat),
      "factor3B" -> of(doubleFormat),
      "factor2B" -> of(doubleFormat),
      "factor1B" -> of(doubleFormat),
      "factorBB" -> of(doubleFormat)
    )(EditParkForm.apply)(EditParkForm.unapply)
  )

}
