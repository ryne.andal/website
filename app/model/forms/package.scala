package model

import model._
import play.api.data.Forms._
import play.api.data.format.Formatter
import play.api.data.{FormError, Mapping}

import scala.util.Try

package object forms {

  val colorFormat: Mapping[Int] = of(
    new Formatter[Int] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], Int] = {
        val parse = if (data(key).startsWith("#")) data(key).substring(1) else key
        Try(Integer.parseInt(parse, 16)).map(Right(_)).getOrElse(Left(Seq(FormError("error.color", "This doesn't look like a hex color."))))
      }

      override def unbind(key: String, value: Int): Map[String, String] = Map(key -> f"#$value%06X")
    }
  )

  val id36Format: Mapping[String] = of(
    new Formatter[String] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], String] = {
        data(key) match {
          case REDDIT_ID36_REGEX(matched) => Right(matched)
          case l: String if l.length >= 6 && l.length <= 7 && l.forall(_.isLetterOrDigit) => Right(l)
          case _ => Left(Seq(FormError("error.id36", "This doesn't look like a reddit thread.")))
        }
      }

      override def unbind(key: String, value: String): Map[String, String] = Map(key -> value)
    }
  )

  val snowflakeFormat: Mapping[String] = text(minLength = 18, maxLength = 18).verifying("This doesn't look like a Discord Snowflake.", _.forall(_.isDigit))

  def playerFormat(implicit db: FBDatabase): Mapping[Player] = of(
    new Formatter[Player] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], Player] = {
        Try(data(key).toInt).fold(
          _ => Left(Seq(FormError("error.player.int", "This is not a valid player."))),
          id => db.getPlayerById(id)
            .map(Right(_))
            .getOrElse(Left(Seq(FormError("error.player.null", "This is not a valid player."))))
        )
      }

      override def unbind(key: String, value: Player): Map[String, String] = Map(key -> value.id.toString)
    }
  )

  def teamFormat(implicit db: FBDatabase): Mapping[Team] = of(
    new Formatter[Team] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], Team] = {
        Try(data(key).toInt).fold(
          _ => Left(Seq(FormError("error.team.int", "This is not a valid team."))),
          id => db.getTeamById(id)
            .map(Right(_))
            .getOrElse(Left(Seq(FormError("error.team.null", "This is not a valid team."))))
        )
      }

      override def unbind(key: String, value: Team): Map[String, String] = Map(key -> value.id.toString)
    }
  )

}
