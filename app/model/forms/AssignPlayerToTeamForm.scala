package model.forms

import model.{FBDatabase, Player, Team}
import play.api.data._
import play.api.data.Forms._

case class AssignPlayerToTeamForm(team: Option[Team], startSeason: Int, startSession: Int)

object AssignPlayerToTeamForm {

  def assignPlayerToTeamForm(implicit db: FBDatabase): Form[AssignPlayerToTeamForm] = Form(
    mapping(
      "team" -> optional(teamFormat),
      "startSeason" -> number(min = 1),
      "startSession" -> number(min = 1)
    )(AssignPlayerToTeamForm.apply)(AssignPlayerToTeamForm.unapply)
  )

}
