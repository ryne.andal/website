package model.forms

import model.{BattingType, FBDatabase, PitchingBonus, PitchingType, Team}
import play.api.data.Forms._
import play.api.data._

case class EditPlayerForm(id: Int, firstName: Option[String], lastName: String, battingType: Option[BattingType], pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus], rightHanded: Boolean, positionPrimary: String, positionSecondary: Option[String], positionTertiary: Option[String])

object EditPlayerForm {

  def editPlayerForm(implicit db: FBDatabase) = Form(
    mapping(
      "id" -> number(min = 1).verifying("This player does not exist.", db.getPlayerById(_).isDefined),
      "firstName" -> optional(nonEmptyText),
      "lastName" -> nonEmptyText,
      "battingType" -> optional(nonEmptyText(minLength = 1, maxLength = 2)).transform[Option[BattingType]](_.flatMap(db.getBattingTypeByShortcode), _.map(_.shortcode)).verifying("This is not a valid batting type.", _.isDefined),
      "pitchingType" -> optional(nonEmptyText(minLength = 2, maxLength = 2)).transform[Option[PitchingType]](_.flatMap(db.getPitchingTypeByShortcode), _.map(_.shortcode)),
      "pitchingBonus" -> optional(nonEmptyText(minLength = 1, maxLength = 1)).transform[Option[PitchingBonus]](_.flatMap(db.getPitchingBonusByShortcode), _.map(_.shortcode)),
      "rightHanded" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "positionPrimary" -> nonEmptyText(minLength = 1, maxLength = 2),
      "positionSecondary" -> optional(nonEmptyText(minLength = 1, maxLength = 2)),
      "positionTertiary" -> optional(nonEmptyText(minLength = 1, maxLength = 2))
    )(EditPlayerForm.apply)(EditPlayerForm.unapply)
  )

}
