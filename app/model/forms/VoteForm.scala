package model.forms

import play.api.data.Forms._
import play.api.data._

case class VoteForm(option1: String, option2: String, option3: String, option4: String, option5: String)

object VoteForm {
  val candidates: Seq[String] = Seq(
    "Valeria Lisova",
    "G.H. Morello",
    "Limón Cristo",
    "Rickey Tucker",
    "Harold Peritestes",
    "Dick McScrotington V",
    "Chuck McCluster",
    "Garren Smudge",
    "Hans Ali",
    "Icna Comit",
    "Tara Dactyl",
    "Dirtbag Darrell",
    "Cal Lidous",
    "Demetrios Ooga",
    "Sael (Buster Hyman)",
    "Johnny Dickshot",
    "Hank Murphy",
    "Dixon Uraz"
  ).sorted

  val voteForm: Form[VoteForm] = Form(
    mapping(
      "option1" -> nonEmptyText().verifying("This is not a valid candidate.", candidates.contains(_)),
      "option2" -> nonEmptyText().verifying("This is not a valid candidate.", candidates.contains(_)),
      "option3" -> nonEmptyText().verifying("This is not a valid candidate.", candidates.contains(_)),
      "option4" -> nonEmptyText().verifying("This is not a valid candidate.", candidates.contains(_)),
      "option5" -> nonEmptyText().verifying("This is not a valid candidate.", candidates.contains(_)),
    )(VoteForm.apply)(VoteForm.unapply)
      .verifying("You have selected the same candidate multiple times.", vf =>
        vf.option1 != vf.option2 && vf.option1 != vf.option3 && vf.option1 != vf.option4 && vf.option1 != vf.option5 &&
        vf.option2 != vf.option3 && vf.option2 != vf.option4 && vf.option2 != vf.option5 &&
        vf.option3 != vf.option4 && vf.option3 != vf.option5 &&
        vf.option4 != vf.option5
      )
  )
}


