package model.forms

import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._

case class ChallongeBracketForm(id: String, theme: Int, multiplier: Float, matchWidthMultiplier: Float, scaleToFit: Boolean, showFinalResults: Boolean, showStandings: Boolean, showVoting: Boolean, tab: Option[String])

object ChallongeBracketForm {

  val challongeBracketForm = Form(
    mapping(
      "id" -> nonEmptyText(maxLength = 100),
      "theme" -> default(number, 1),
      "multiplier" -> default(of(floatFormat), 1f).verifying("Value must be between 0.3 and 3.0", f => f >= 0.3 && f <= 3.0),
      "matchWidthMultiplier" -> default(of(floatFormat), 1f).verifying("Value must be between 0.5 and 2.0", f => f >= 0.5 && f <= 2.0),
      "scaleToFit" -> default(boolean, false),
      "showFinalResults" -> default(boolean, false),
      "showStandings" -> default(boolean, false),
      "showVoting" -> default(boolean, false),
      "tab" -> optional(text)
    )(ChallongeBracketForm.apply)(ChallongeBracketForm.unapply)
  )

}
