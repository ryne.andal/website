package model.forms

import play.api.data.Forms._
import play.api.data._

case class CreatePlayerForm(redditName: String, firstName: Option[String], lastName: String, team: Option[Int], battingType: Int, pitchingType: Option[Int], pitchingBonus: Option[Int], rightHanded: Boolean, positionPrimary: String, positionSecondary: Option[String])

object CreatePlayerForm {

  val createPlayerForm = Form(
    mapping(
      "redditName" -> nonEmptyText(minLength = 3, maxLength = 23),
      "firstName" -> optional(nonEmptyText(maxLength = 15)),
      "lastName" -> nonEmptyText(maxLength = 15),
      "team" -> optional(number(min = 1)),
      "battingType" -> number(min = 1),
      "pitchingType" -> optional(number(min = 1)),
      "pitchingBonus" -> optional(number(min = 1)),
      "rightHanded" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "positionPrimary" -> nonEmptyText(maxLength = 2),
      "positionSecondary" -> optional(nonEmptyText(maxLength = 2))
    )(CreatePlayerForm.apply)(CreatePlayerForm.unapply)
  )

}