package model.forms

import play.api.data.Forms._
import play.api.data._

case class SetID36Form(id36: String, owner: Option[Int])

object SetID36Form {

  val setID36Form = Form(
    mapping(
      "id36" -> id36Format,
      "owner" -> optional(number)
    )(SetID36Form.apply)(SetID36Form.unapply)
  )

}
