package model.forms

import model.{FBDatabase, availablePositions}
import play.api.data.Forms._
import play.api.data._

case class NewPlayerForm(returning: Boolean, firstName: Option[String], lastName: String, willingToJoinDiscord: Boolean, discordName: Option[String], positionPrimary: String, positionSecondary: Option[String], rightHanded: Boolean, battingType: String, pitchingType: Option[String], pitchingBonus: Option[String], agreeToTerms: Boolean) {

  def validatePositions(): Boolean = {
    if (positionPrimary == "P")
      positionSecondary.isEmpty
    else
      positionSecondary.exists { pos =>
        NewPlayerForm.secondaryPositionMap
          .getOrElse(positionPrimary, Set())
          .contains(pos)
      }
  }

  def validatePlayerTypes()(implicit db: FBDatabase): Boolean = {
    db.getBattingTypeByShortcode(battingType).isDefined && {
      if (positionPrimary != "P")
        pitchingType.isEmpty &&
          pitchingBonus.isEmpty
      else
        pitchingType.exists { t => db.getPitchingTypeByShortcode(t).isDefined } &&
          pitchingBonus.exists { t => db.getPitchingBonusByShortcode(t).isDefined }
    }
  }

}

object NewPlayerForm {

  val secondaryPositionMap: Map[String, Set[String]] = Map(
    "C" -> Set("1B", "3B"),
    "1B" -> Set("3B", "LF"),
    "2B" -> Set("SS", "1B", "RF"),
    "3B" -> Set("2B", "SS", "LF"),
    "SS" -> Set("2B", "3B", "CF"),
    "LF" -> Set("RF", "1B"),
    "CF" -> Set("LF", "RF"),
    "RF" -> Set("CF", "1B")
  )

  val availableBattingTypes: Seq[(String, String)] = Seq(
    "BN" -> "Basic Neutral",
    "BP" -> "Basic Power",
    "BC" -> "Basic Contact",
    "S" -> "Speedy",
    "1B" -> "1B/BB",
    "WC" -> "Work the Count",
    "SM" -> "Sacrifice Master",
    "SF" -> "Single Focused",
    "EN" -> "Extremely Neutral",
    "XB" -> "Extra Base Focus",
    "TT" -> "Three True Outcomes",
    "HK" -> "HR/K",
    "MH" -> "Max Homers"
  )

  val availablePitchingTypes: Seq[(String, String)] = Seq(
    "BB" -> "Basic Balanced",
    "BS" -> "Basic Strikeout",
    "BF" -> "Basic Finesse",
    "NH" -> "No Homers",
    "TD" -> "Trust Your Defense",
    "1B" -> "1B/BB",
    "WC" -> "Weak Contact",
    "EG" -> "Extreme Groundballer",
    "SF" -> "Single Focused",
    "NT" -> "Nothing to Hit",
    "EN" -> "Extremely Neutral",
    "FP" -> "Flyball Pitcher",
    "TT" -> "Three True Outcomes"
  )

  val availablePitchingBonuses: Seq[(String, String)] = Seq(
    "H" -> "Anti-Homers",
    "S" -> "Anti-Single",
    "B" -> "Balanced"
  )

  def newPlayerForm(implicit db: FBDatabase) = Form(
    mapping(
      "returning" -> boolean,
      "firstName" -> optional(text(minLength = 1, maxLength = 15)),
      "lastName" -> text(minLength = 1, maxLength = 15),
      "willingToJoinDiscord" -> boolean,
      "discordName" -> optional(nonEmptyText),
      "positionPrimary" -> text(minLength = 1, maxLength = 2).verifying("This is not a valid position.", availablePositions.contains _),
      "positionSecondary" -> optional(text(minLength = 1, maxLength = 2)),
      "rightHanded" -> boolean,
      "battingType" -> text(minLength = 1, maxLength = 2).verifying("This is not a valid batting type.", availableBattingTypes.map(_._1).contains _),
      "pitchingType" -> optional(text(minLength = 1, maxLength = 2).verifying("This is not a valid pitching type.", availablePitchingTypes.map(_._1).contains _)),
      "pitchingBonus" -> optional(text(minLength = 1, maxLength = 2).verifying("This is not a valid pitching bonus.", availablePitchingBonuses.map(_._1).contains _)),
      "agreeToTerms" -> checked("You must agree to these terms.")
    )(NewPlayerForm.apply)(NewPlayerForm.unapply)
      .verifying("This name is already taken.", form => db.getPlayerBySplitName(form.firstName, form.lastName).isEmpty)
      .verifying("That is not a valid secondary position for the selected primary position.", _.validatePositions())
      .verifying("Your batting/pitching types are not valid.", _.validatePlayerTypes())
  )
}
