package model.forms

import model._
import play.api.data.Forms._
import play.api.data._

case class NewGameActionForm(batter: Int, swing: Option[Int], pitcher: Int, pitch: Option[Int], playType: Int, save: Boolean)

object NewGameActionForm {

  def newGameActionForm(implicit db: FBDatabase): Form[NewGameActionForm] = Form(
    mapping(
      "batter" -> number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined),
      "swing" -> optional(number(min = 1, max = 1000)),
      "pitcher" -> number(min = 1).verifying("This is not a valid player.", db.getPlayerById(_).isDefined),
      "pitch" -> optional(number(min = 1, max = 1000)),
      "playType" -> number(min = PLAY_TYPES.values.min, max = PLAY_TYPES.values.max),
      "save" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_))
    )(NewGameActionForm.apply)(NewGameActionForm.unapply)
      .verifying("You must provide a pitch for this play type.", form => form.pitch.isDefined || form.playType == PLAY_TYPE_AUTO_K || form.playType == PLAY_TYPE_AUTO_BB || form.playType == PLAY_TYPE_IBB)
      .verifying("You must provide a swing for this play type.", form => form.swing.isDefined || form.playType == PLAY_TYPE_AUTO_K || form.playType == PLAY_TYPE_AUTO_BB || form.playType == PLAY_TYPE_IBB)
  )

}
