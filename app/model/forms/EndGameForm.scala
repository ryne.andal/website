package model.forms

import play.api.data.Forms._
import play.api.data._

case class EndGameForm(gameId: Int)

object EndGameForm {

  val endGameForm = Form(
    mapping(
      "gameId" -> number(min = 1)
    )(EndGameForm.apply)(EndGameForm.unapply)
  )

}
