package model

import java.sql.{Date, Timestamp}
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.Tables._
import model.forms.{AssignPlayerToTeamForm, ChallongeBracketForm, CreatePlayerForm, EditParkForm, EditPlayerForm, EditTeamForm, GameAwardsForm, NewGameForm, NewPlayerForm, UserPreferencesForm, VoteForm}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.MySQLProfile
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.language.implicitConversions

@Singleton
class FBDatabase @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[MySQLProfile] {

  private val UserWithPlayerQuery = Users
    .joinLeft(Players).on(_.id === _.user)
  private val PlayerWithUserQuery = Players
    .join(Users).on(_.user === _.id)
  private val UsersWithPreferencesQuery = Users
    .join(UserPreferences).on(_.id === _.user)
  private val GMAssignmentWithPlayerQuery = GmAssignments
    .join(Players).on(_.gm === _.id)
  private val PlayerWithTypesQuery = Players
    .join(BattingTypes).on(_.battingType === _.id)
    .joinLeft(PitchingTypes).on(_._1.pitchingType === _.id)
    .joinLeft(PitchingBonuses).on(_._1._1.pitchingBonus === _.id)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))
  private val PlayerWithTypesAndInfoQuery = PlayerWithTypesQuery
    .joinLeft(Teams).on(_._1.team === _.id)
    .join(Users).on(_._1._1.user === _.id)
    .map(all => (all._1._1._1, all._2, all._1._2, all._1._1._2, all._1._1._3, all._1._1._4))
  private val GameWithTeamsQuery = Games
    .join(Teams).on(_.awayTeam === _.id)
    .join(Teams).on(_._1.homeTeam === _.id)
    .join(Parks).on(_._1._1.park === _.id)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))
  private val GameWithTeamsAndStateQuery = GameWithTeamsQuery
    .join(GameStates).on(_._1.state === _.id)
    .map(all => (all._1._1, all._1._2, all._1._3, all._1._4, all._2))

  private def PlayerWithStatsQuery(season: Int, milr: Boolean) = Players
    .join(Users).on(_.user === _.id)
    .joinLeft(BattingStats.filter(bs => bs.season === season && bs.milr === milr)).on(_._1.id === _.player)
    .joinLeft(PitchingStats.filter(ps => ps.season === season && ps.milr === milr)).on(_._1._1.id === _.player)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))

  private def LineupEntriesWithPlayers(season: Int, milr: Boolean) = LineupEntries
    .join(PlayerWithStatsQuery(season, milr)).on(_.player === _._1.id)
    .map(all => (all._1, all._2))

  private val GameActionsWithPlayersQuery = GameActions
    .join(Players).on(_.batter === _.id)
    .join(Players).on(_._1.pitcher === _.id)
    .map(all => (all._1._1, all._1._2, all._2))
  private val GameActionsWithStatesAndPlayersQuery = GameActionsWithPlayersQuery
    .join(GameStates).on(_._1.beforeState === _.id)
    .join(GameStates).on(_._1._1.afterState === _.id)
    .map(all => (all._1._1._1, all._1._1._2, all._1._1._3, all._1._2, all._2))
  private val GameActionsWithExtendedInfo = GameActionsWithStatesAndPlayersQuery
    .join(Games).on(_._1.gameId === _.id)
    .join(Users).on(_._1._2.user === _.id)
    .join(Users).on(_._1._1._3.user === _.id)
    .join(Teams).on(_._1._1._2.awayTeam === _.id)
    .join(Teams).on(_._1._1._1._2.homeTeam === _.id)
    .join(Parks).on(_._1._1._1._1._2.park === _.id)
    .map(all => (all._1._1._1._1._1._1._1, all._1._1._1._1._1._1._2, all._1._1._1._1._1._1._3, all._1._1._1._1._1._1._4, all._1._1._1._1._1._1._5, all._1._1._1._1._1._2, all._1._1._1._1._2, all._1._1._1._2, all._1._1._2, all._1._2, all._2))
  private val ScoringPlaysWithActionsAndStatesQuery = ScoringPlays
    .join(GameActions).on(_.gameAction === _.id)
    .join(GameStates).on(_._2.beforeState === _.id)
    .join(GameStates).on(_._1._2.afterState === _.id)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))
  private val TeamsWithParksQuery = Teams
    .join(Parks).on(_.park === _.id)
  private val ParksWithTeamsQuery = Parks
    .joinLeft(Teams).on(_.id === _.park)
  private val GameAwardsWithPlayersQuery = GameAwards
    .join(Players).on(_.winningPitcher === _.id)
    .join(Players).on(_._1.losingPitcher === _.id)
    .join(Players).on(_._1._1.playerOfTheGame === _.id)
    .joinLeft(Players).on(_._1._1._1.save === _.id)
    .map(all => (all._1._1._1._1, all._1._1._1._2, all._1._1._2, all._1._2, all._2))

  def awaitDB[R](awaitable: DBIOAction[R, NoStream, Nothing]): R = {
    Await.result(db.run(awaitable), Duration(5, TimeUnit.SECONDS))
  }

  def getUserByRedditName(reddit: String): Option[User] = awaitDB {
    Users.filter(_.redditName === reddit).take(1).result.headOption
  }

  def getAllParks: Seq[Park] = awaitDB {
    Parks
      .result
  }

  def getPlayerByRedditName(reddit: String): Option[Player] = awaitDB {
    (for {
      u <- Users if u.redditName === reddit
      p <- Players if p.user === u.id && !p.deactivated
    } yield p).take(1).result.headOption
  }

  def getPlayerByName(name: String): Option[Player] = awaitDB {
    Players.filter(_.name === name).take(1).result.headOption
  }

  def searchPlayers(name: String): Seq[PlayerWithTypesAndInfo] = awaitDB {
    PlayerWithTypesAndInfoQuery
      .filter(p => p._1.name.like(s"%$name%") || p._2.redditName.like(s"%$name%"))
      .take(10)
      .result
  }.map(tuple2PlayerWithTypesAndInfo)

  def getPlayerBySnowflake(snowflake: String): Option[PlayerWithTypesAndInfo] = awaitDB {
    PlayerWithTypesAndInfoQuery
      .filter(p => p._2.discord === snowflake)
      .take(1)
      .result
      .headOption
  }.map(tuple2PlayerWithTypesAndInfo)

  def getPlayersWithInfoOnTeam(team: Team): Seq[PlayerWithTypesAndInfo] = awaitDB {
    if (team.milr) {
      PlayerWithTypesAndInfoQuery
        .filter(p => p._1.team === team.id || p._3.flatMap(_.milrTeam) === team.id)
        .result
    } else {
      PlayerWithTypesAndInfoQuery
        .filter(p => p._1.team === team.id)
        .result
    }
  }.map(tuple2PlayerWithTypesAndInfo)

  def getPitchingRecords(player: Player): Map[(Boolean, Int), (Int, Int)] = awaitDB {
    sql"""select a.milr,a.season,sum(case when b.winning_pitcher = ${player.id} then 1 else 0 end),sum(case when b.losing_pitcher = ${player.id} then 1 else 0 end)
          from games a, game_awards b
          where (b.winning_pitcher = ${player.id} or b.losing_pitcher = ${player.id}) and b.game = a.id and a.stats_calculated
          group by a.milr, a.season"""
      .as[(Boolean, Int, Int, Int)]
  }.map(a => ((a._1, a._2), (a._3, a._4))).toMap

  def searchTeams(query: String): Seq[TeamWithPark] = awaitDB {
    TeamsWithParksQuery
      .filter(t => t._1.name.like(s"%$query%") || t._1.tag.like(s"%$query%"))
      .take(10)
      .result
  }.map(tuple2TeamWithPark)

  def searchParks(query: String): Seq[Park] = awaitDB {
    if (query.length == 3)
      ParksWithTeamsQuery
        .filter(t => t._2.map(_.tag) === query)
        .map(all => all._1)
        .result
    else
      ParksWithTeamsQuery
        .filter(t => t._1.name.like(s"%$query%"))
        .map(all => all._1)
        .result
  }.groupBy(_.id).map(_._2.head).toSeq.take(10)

  def getAllParksWithTeams: Seq[ParkWithTeam] = awaitDB {
    ParksWithTeamsQuery
      .result
  }.map(tuple2ParkWithTeam).groupBy(_.park.id).map(_._2.head).toSeq

  def getPlayerBySplitName(firstName: Option[String], lastName: String): Option[Player] = awaitDB {
    Players
      .filterNot(_.deactivated)
      .filter(_.firstName === firstName)
      .filter(_.lastName === lastName)
      .take(1)
      .result
      .headOption
  }

  def getPlayerById(id: Int): Option[Player] = awaitDB {
    Players.filter(_.id === id).take(1).result.headOption
  }

  def createPlayer(user: Int, form: CreatePlayerForm): Int = awaitDB {
    Players.returning(Players.map(_.id)) += PlayersRow(0, user, s"${form.firstName.getOrElse("")} ${form.lastName}".trim, form.firstName, form.lastName, form.team, form.battingType, form.pitchingType, form.pitchingBonus, form.rightHanded, form.positionPrimary, form.positionSecondary, None, deactivated = false)
  }

  def getPlayerFull(id: Int): Option[PlayerWithTypesAndInfo] = awaitDB {
    PlayerWithTypesAndInfoQuery
      .filter(_._1.id === id)
      .take(1)
      .result
      .headOption
  }.map(tuple2PlayerWithTypesAndInfo)

  def getPlayerWithStatsById(id: Int, season: Int, milr: Boolean): Option[PlayerWithStats] = awaitDB {
    PlayerWithStatsQuery(season, milr)
      .filter(_._1.id === id)
      .take(1)
      .result
      .headOption
  }.map(tuple2PlayerWithStats)

  def getPlayerWithAllStatsById(id: Int): Option[PlayerWithAllStats] = {
    // Find the player first
    val player = awaitDB {
      PlayerWithUserQuery
        .filter(_._1.id === id)
        .take(1)
        .result
        .headOption
    }

    // Now grab all the stats
    player.map { player =>
      val bStats = awaitDB(BattingStats.filter(_.player === player._1.id).result)
        .map(stats => (stats.milr, stats.season) -> battingStatHList2BattingStatSet(stats)).toMap
      val pStats = awaitDB(PitchingStats.filter(_.player === player._1.id).result)
        .map(stats => (stats.milr, stats.season) -> stats).toMap
      PlayerWithAllStats(player._1, player._2, bStats, pStats)
    }
  }

  def getBattingTypeByShortcode(code: String): Option[BattingType] = awaitDB {
    BattingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingTypeByShortcode(code: String): Option[PitchingType] = awaitDB {
    PitchingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingBonusByShortcode(code: String): Option[PitchingBonus] = awaitDB {
    PitchingBonuses.filter(_.shortcode === code).take(1).result.headOption
  }

  def getBattingTypeById(id: Int): BattingType = awaitDB {
    BattingTypes.filter(_.id === id).take(1).result.head
  }

  def getPitchingTypeById(id: Int): PitchingType = awaitDB {
    PitchingTypes.filter(_.id === id).take(1).result.head
  }

  def getPitchingBonusById(id: Int): PitchingBonus = awaitDB {
    PitchingBonuses.filter(_.id === id).take(1).result.head
  }

  def createPlayerApplication(application: NewPlayerForm)(implicit user: User): PlayerApplication = awaitDB {
    val bt = getBattingTypeByShortcode(application.battingType).get.id
    val pt = application.pitchingType.map(pt => getPitchingTypeByShortcode(pt).get.id)
    val pb = application.pitchingBonus.map(pb => getPitchingBonusByShortcode(pb).get.id)
    PlayerApplications returning PlayerApplications.map(_.id) into ((app, newId) => app.copy(id = newId)) += PlayerApplicationsRow(0, user.id, application.lastName, application.firstName, application.returning, application.willingToJoinDiscord, application.discordName, application.positionPrimary, application.positionSecondary, application.rightHanded, bt, pt, pb, 0)
  }

  def getPlayerApplicationByUser(user: User): Option[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.user === user.id).take(1).result.headOption
  }

  def getPlayerApplicationById(id: Int): Option[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.id === id).take(1).result.headOption
  }

  def updatePlayerApplicationWithResponse(id: Int, newStatus: Int, rejectionMessage: Option[String], user: Int): Unit = awaitDB {
    PlayerApplications.filter(_.id === id)
      .map(app => (app.status, app.respondedAt, app.respondedBy, app.rejectMessage))
      .update((newStatus, Some(new Timestamp(System.currentTimeMillis())), Some(user), rejectionMessage))
  }

  def createNewPlayer(app: PlayerApplication): Player = {
    val newPlayer = awaitDB(Players returning Players.map(_.id) into ((player, newId) => player.copy(id = newId)) += PlayersRow(0, app.user, s"${app.firstName.getOrElse("")} ${app.lastName}".trim, app.firstName, app.lastName, None, app.battingType, app.pitchingType, app.pitchingBonus, app.isRightHanded, app.positionPrimary, app.positionSecondary, None, deactivated = false))
    newPlayer
  }

  def editPlayerApplication(appid: Int, application: NewPlayerForm): Int = awaitDB {
    val bt = getBattingTypeByShortcode(application.battingType).get.id
    val pt = application.pitchingType.map(pt => getPitchingTypeByShortcode(pt).get.id)
    val pb = application.pitchingBonus.map(pb => getPitchingBonusByShortcode(pb).get.id)
    PlayerApplications.filter(_.id === appid)
      .map(app => (app.status, app.firstName, app.lastName, app.isReturningPlayer, app.isWillingToJoinDiscord, app.positionPrimary, app.positionSecondary, app.isRightHanded, app.battingType, app.pitchingType, app.pitchingBonus))
      .update((0, application.firstName, application.lastName, application.returning, application.willingToJoinDiscord, application.positionPrimary, application.positionSecondary, application.rightHanded, bt, pt, pb))
  }

  def getMLRTeams: Seq[Team] = awaitDB {
    Teams.filterNot(_.milr).sortBy(_.name).result
  }

  def getMiLRTeams: Seq[Team] = awaitDB {
    Teams.filter(_.milr).sortBy(_.name).result
  }

  def getAllTeams: Seq[Team] = awaitDB {
    Teams.sortBy(t => (t.milr, t.name)).result
  }

  def getTeamById(id: Int): Option[Team] = awaitDB {
    Teams
      .filter(_.id === id)
      .take(1)
      .result
      .headOption
  }

  def editTeam(id: Int, form: EditTeamForm): Unit = awaitDB {
    Teams
      .filter(_.id === id)
      .map(t => (t.tag, t.name, t.park, t.discordRole, t.colorDiscord, t.colorRoster, t.colorRosterBg, t.milr, t.milrTeam))
      .update((form.tag, form.name, form.park, form.discordRole, form.colorDiscord, form.colorRoster, form.colorRosterBg, form.milr, form.milrTeam))
  }

  def createTeam(form: EditTeamForm): Int = awaitDB {
    Teams returning Teams.map(_.id) += new Team(0, form.tag, form.name, form.park, form.discordRole, form.colorDiscord, form.colorRoster, form.colorRosterBg, form.milr, form.milrTeam)
  }

  def getPlayersOnTeam(team: Team): Seq[Player] = awaitDB {
    if (team.milr)
      Players
        .join(Teams).on(_.team === _.id)
        .filter(_._2.milrTeam === team.id)
        .map(_._1)
        .result
    else
      Players
        .filter(_.team === team.id)
        .result
  }

  def getFreeAgents: Seq[PlayerWithTypes] = awaitDB {
    PlayerWithTypesQuery
      .filter(_._1.team.isEmpty)
      .filter(!_._1.deactivated)
      .result
  }.map(tuple2PlayerWithTypes)

  def getUserById(id: Int): Option[User] = awaitDB {
    Users.filter(u => u.id === id).take(1).result.headOption
  }

  def getUserWithPreferencesById(id: Int): Option[UserWithPreferences] = awaitDB {
    UsersWithPreferencesQuery.filter(_._1.id === id).take(1).result.headOption
  }.map(tuple2UserWithPreferences)

  def updateUserPreferences(id: Int, newPreferences: UserPreferencesForm): Unit = awaitDB {
    UserPreferences
      .filter(_.user === id)
      .map(p => p.umpBatterPing)
      .update(newPreferences.umpBatterPing)
  }

  def createUserAccount(redditName: String): Int = {
    val id = awaitDB(Users returning Users.map(_.id) += UsersRow(0, redditName))
    awaitDB(UserPreferences.map(_.user) += id)
    id
  }

  def setDiscordSnowflake(user: User, snowflake: Option[String]): Unit = awaitDB {
    Users
      .filter(_.id === user.id)
      .map(_.discord)
      .update(snowflake)
  }

  def setGoogleId(user: User, id: Option[String]): Unit = awaitDB {
    Users
      .filter(_.id === user.id)
      .map(_.googleId)
      .update(id)
  }

  def getUserByGoogle(id: String): Option[User] = awaitDB {
    Users
      .filter(_.googleId === id)
      .take(1)
      .result
      .headOption
  }

  def assignGMToTeam(team: Team, newGM: Player, oldGMEndReason: Option[String], season: Int, session: Int): Unit = awaitDB {
    // Swap out GM by ending old one and starting new one
    DBIO.seq(
      GmAssignments.filter(_.team === team.id)
        .filter(_.endSeason.isEmpty)
        .map(t => (t.endSeason, t.endSession, t.endReason))
        .update(Some(season), Some(session), oldGMEndReason),
      GmAssignments += GmAssignmentsRow(0, team.id, newGM.id, season, session)
    )
  }

  def getGMOfTeam(team: Team): Option[GMAssignmentWithPlayer] = awaitDB {
    GMAssignmentWithPlayerQuery
      .filter(gma => gma._1.team === team.id && gma._1.endSeason.isEmpty)
      .take(1)
      .result
      .headOption
  }.map(tuple2GMAssignmentWithPlayer)

  def countPendingPlayerApplications(): Int = awaitDB {
    PlayerApplications.filter(_.status === 0).length.result
  }

  def getAllPendingApplications: Seq[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.status === 0).result
  }

  def getGameByTeamTag(season: Int, session: Int, team: String): Option[GamesRow] = awaitDB {
    (for {
      t <- Teams if t.tag === team
      g <- Games if g.season === season && g.session === session && (g.homeTeam === t.id || g.awayTeam === t.id)
    } yield g).take(1).result.headOption
  }

  def getActiveGameForTeam(team: String): Option[GameWithGameState] = {
    val list = awaitDB {
      GameWithTeamsAndStateQuery
        .filter(g => !g._1.completed && (g._2.tag === team || g._3.tag === team))
        .result
    }
    if (list.size > 1)
      list.sortBy(_._1.session).headOption
    else
      list.headOption
  }.map(tuple2GameWithGameState)

  def getTeamByTag(tag: String): Option[Team] = awaitDB {
    Teams.filter(_.tag === tag).take(1).result.headOption
  }

  def createGame(form: NewGameForm): Game = awaitDB {
    val awayTeamId = getTeamByTag(form.awayTeam).get.id
    val homeTeam = getTeamByTag(form.homeTeam).get
    val homeTeamId = homeTeam.id
    val parkId = form.park.getOrElse(homeTeam.park)
    val newState = awaitDB(GameStates returning GameStates.map(_.id) += new GameState(0, 1, 1, None, None, None, None, None, None, 0, 1, 0, 0))
    Games returning Games.map(_.id) into ((game, newId) => game.copy(id = newId)) += GamesRow(0, form.season, form.session, form.milr, awayTeamId, homeTeamId, form.id36, gameStart = new java.sql.Date(form.startDate.getTime), statsCalculated = form.statsCalculation, state = newState, park = parkId)
  }

  def markGameComplete(gameId: Int): Unit = awaitDB {
    Games
      .filter(_.id === gameId)
      .map(_.completed)
      .update(true)
  }

  def setID36(game: Game, id36: String, threadOwner: Option[Int]): Unit = awaitDB {
    Games
      .filter(_.id === game.id)
      .map(g => (g.id36, g.threadOwner))
      .update(Some(id36), threadOwner)
  }

  def getAllActiveUmpires: Seq[UserWithPlayer] = awaitDB {
    (for {
      (u, p) <- Users.filter(_.isUmpire)
        .joinLeft(Players).on(_.id === _.user)
    } yield (u, p)).result
  }.map(tuple2UserWithPlayer)

  def setUmpireStatus(userId: Int, isUmpire: Boolean): Unit = awaitDB {
    Users.filter(_.id === userId)
      .map(_.isUmpire)
      .update(isUmpire)
  }

  def getGamesWithUmpAssignments: Seq[(GameWithTeams, Option[UserWithPlayer])] = awaitDB {
    GameWithTeamsQuery.filterNot(_._1.completed)
      .joinLeft(UmpireAssignments.join(UserWithPlayerQuery).on(_.umpire === _._1.id)).on(_._1.id === _._1.game)
      .map(all => (all._1, all._2.map(_._2))).result
  }.map(all => (tuple2GameWithTeams(all._1), all._2.map(tuple2UserWithPlayer)))

  def isUmpireAssignedToGame(umpire: User, gameId: Int): Boolean = awaitDB {
    UmpireAssignments.filter(ua => ua.umpire === umpire.id && ua.game === gameId).result.headOption
  }.isDefined

  def assignUmpireToGame(umpire: User, gameId: Int): Unit = awaitDB {
    UmpireAssignments += UmpireAssignmentsRow(gameId, umpire.id)
  }

  def removeUmpireFromGame(umpire: User, gameId: Int): Unit = awaitDB {
    UmpireAssignments.filter(ua => ua.umpire === umpire.id && ua.game === gameId).delete
  }

  def getGameById(id: Int): Option[Game] = awaitDB {
    Games.filter(_.id === id).take(1).result.headOption
  }

  def getGamesForUmpire(umpire: User): Seq[GameWithGameState] = awaitDB {
    UmpireAssignments
      .filter(_.umpire === umpire.id)
      .join(GameWithTeamsAndStateQuery).on(_.game === _._1.id)
      .map(all => all._2)
      .result
  }.map(tuple2GameWithGameState)

  def getUmpiresForGame(game: Int): Seq[UserWithPlayer] = awaitDB {
    UmpireAssignments
      .join(UserWithPlayerQuery).on(_.umpire === _._1.id)
      .filter(_._1.game === game)
      .map(all => all._2)
      .result
  }.map(tuple2UserWithPlayer)

  def getGamesInSession(season: Int, session: Int): Seq[GameWithGameState] = awaitDB {
    GameWithTeamsAndStateQuery
      .filter(g => g._1.season === season && g._1.session === session)
      .result
  }.map(tuple2GameWithGameState)

  def getActiveGames: Seq[GameWithGameState] = awaitDB {
    GameWithTeamsAndStateQuery
      .filter(g => !g._1.completed)
      .result
  }.map(tuple2GameWithGameState)

  def getUnfinishedGames: Seq[GameWithTeams] = awaitDB {
    GameWithTeamsQuery
      .filterNot(_._1.completed)
      .result
  }.map(tuple2GameWithTeams)

  def getLineupWithPlayers(lineupId: Int, game: Game): Seq[LineupEntryWithPlayer] = awaitDB {
    LineupEntriesWithPlayers(game.season, game.milr)
      .filter(_._1.lineup === lineupId)
      .sortBy(_._1.battingPos)
      .result
  }.map(tuple2LineupWithPlayers)

  def getGameWithInfoById(gameId: Int): Option[GameWithGameState] = awaitDB {
    GameWithTeamsAndStateQuery
      .filter(_._1.id === gameId)
      .take(1)
      .result
      .headOption
  }.map(tuple2GameWithGameState)

  def createLineup: Lineup = awaitDB {
    Lineups returning Lineups.map(_.id) into ((lineup, newId) => lineup.copy(id = newId)) += LineupsRow(0)
  }

  def setLineup(game: Game, lineup: Lineup, awayTeam: Boolean): Unit = awaitDB {
    Games.filter(_.id === game.id)
      .map(g => if (awayTeam) g.awayLineup else g.homeLineup)
      .update(Some(lineup.id))
  }

  def addLineupEntry(lineup: Int, player: Int, position: String, battingPos: Int, replaces: Option[Int] = None): Unit = {
    val newId = awaitDB {
      LineupEntries returning LineupEntries.map(_.id) += LineupEntriesRow(0, lineup, player, position, battingPos, None, new Timestamp(System.currentTimeMillis()))
    }
    replaces.map(replaces => awaitDB {
      LineupEntries.filter(_.id === replaces).map(_.replacedBy).update(Some(newId))
    })
  }

  def replaceLineupEntryPosition(lineupEntry: LineupEntry, newPosition: String): Unit = awaitDB {
    LineupEntries
      .filter(_.id === lineupEntry.id)
      .map(_.position)
      .update(newPosition)
  }

  def updateStateForExtras(state: GameState, r1: Option[Int], r2: Option[Int], r3: Option[Int]): Unit = awaitDB {
    GameStates
      .filter(_.id === state.id)
      .map(s => (s.r1, s.r2, s.r3))
      .update(r1, r2, r3)
  }

  def addGameAction(game: Game, partialAction: PartialGameAction, newState: GameState, scoringPlay: Option[ScoringPlay]): GameState = {
    // Add the new state
    val newStateId = awaitDB(GameStates returning GameStates.map(_.id) += newState)
    // Update the action's before and after states, then import
    val newAction = partialAction.toGameAction(game, game.state, newStateId)
    val newId = awaitDB(GameActions returning GameActions.map(_.id) += newAction)
    // Now update the old action's replaced by field
    awaitDB(GameActions.filter(action => action.gameId === newAction.gameId && action.replacedBy.isEmpty && action.id =!= newId).map(_.replacedBy).update(Some(newId)))
    // And also update the game's state pointer
    awaitDB(Games.filter(_.id === game.id).map(_.state).update(newStateId))
    // Finally track the scoring play if needed
    if (scoringPlay.isDefined) {
      val play = scoringPlay.get.copy(gameAction = newId, gameId = game.id)
      awaitDB(ScoringPlays += play)
    }

    // Update player stats if applicable
    if (game.statsCalculated) {
      // Collect unique hitters (batter + baserunners)
      Set(
        scoringPlay.flatMap(_.scorer1),
        scoringPlay.flatMap(_.scorer2),
        scoringPlay.flatMap(_.scorer3),
        newAction.batter
      )
          .filter(_.isDefined)
          .map(_.get)
          .foreach(recalculateBattingStatsForPlayer(_, game.season, game.milr))

      // And unique pitchers (current and scorer placers)
      Set(
        scoringPlay.flatMap(_.scorer1Responsibility),
        scoringPlay.flatMap(_.scorer2Responsibility),
        scoringPlay.flatMap(_.scorer3Responsibility),
        newAction.pitcher
      )
        .filter(_.isDefined)
        .map(_.get)
        .foreach(recalculatePitchingStatsForPlayer(_, game.season, game.milr))
    }

    newState.copy(id = newStateId)
  }

  def recalculateBattingStatsForPlayer(player: Int, season: Int, milr: Boolean): Unit = awaitDB {
    if (awaitDB(BattingStats.filter(_.player === player).filter(_.season === season).filter(_.milr === milr).take(1).result.headOption).isEmpty)
      awaitDB(BattingStats.map(row => (row.player, row.season, row.milr)) += (player, season, milr))
    sqlu"""
          update batting_stats t
          set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 9),
              total_abs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and play_type != 8 and play_type != 9 and result != '' and result != 'BB' and !(result = 'FO' and runs_scored = 1) and result != 'Bunt Sac'),
              total_rbi=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 8 and play_type != 9 and play_type != 3 and result != 'BB' and outs_tracked < 2),
              total_r=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and FIND_IN_SET(t.player, a.scorers) != 0),
              total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = 'HR'),
              total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = '3B'),
              total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = '2B'),
              total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = '1B'),
              total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and (result = 'BB' or result = 'Auto BB' or result = 'IBB')),
              total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = 'FO'),
              total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and (result = 'K' or result = 'Auto K' or result = 'Bunt K')),
              total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = 'PO'),
              total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = 'RGO'),
              total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result = 'LGO'),
              total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
              total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home')),
              total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and outs_tracked=2),
              total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and outs_tracked=3),
              total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and a.player = t.player and a.batting_pos != 0),
              total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result='FO' and runs_scored=1)
              where player=$player and season=$season and milr=$milr
      """
  }

  def recalculatePitchingStatsForPlayer(player: Int, season: Int, milr: Boolean): Unit = awaitDB {
    if (awaitDB(PitchingStats.filter(_.player === player).filter(_.season === season).filter(_.milr === milr).take(1).result.headOption).isEmpty)
      awaitDB(PitchingStats.map(row => (row.player, row.season, row.milr)) += (player, season, milr))
    sqlu"""
          update pitching_stats t
          set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and play_type != 6 and play_type != 7 and play_type != 9),
              total_outs=(select (case when sum(outs_tracked) is null then 0 else sum(outs_tracked) end) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and play_type != 3 and play_type != 9 and result != ''),
              total_er=(case when (select sum(case when scorer1_responsibility = $player then 1 else 0 end) + sum(case when scorer2_responsibility = $player then 1 else 0 end) + sum(case when scorer3_responsibility = $player then 1 else 0 end) + sum(case when scorer4_responsibility = $player then 1 else 0 end) from scoring_plays where game_id in (select id from games g where season = $season and g.stats_calculated and (select id from teams where id = g.home_team and milr=$milr) is not null)) is null then 0 else ((select sum(case when scorer1_responsibility = $player then 1 else 0 end) + sum(case when scorer2_responsibility = $player then 1 else 0 end) + sum(case when scorer3_responsibility = $player then 1 else 0 end) + sum(case when scorer4_responsibility = $player then 1 else 0 end) from scoring_plays where game_id in (select id from games g where season = $season and g.stats_calculated and (select id from teams where id = g.home_team and milr=$milr) is not null))) end),
              total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'HR'),
              total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = '3B'),
              total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = '2B'),
              total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = '1B'),
              total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and (result = 'BB' or result = 'Auto BB' or result = 'IBB')),
              total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'FO'),
              total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'K'),
              total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'PO'),
              total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'RGO'),
              total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and result = 'LGO'),
              total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
              total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and pitcher = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home')),
              total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and outs_tracked=2),
              total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and outs_tracked=3),
              total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and a.player = t.player and a.batting_pos = 0),
              total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season and b.milr=$milr) is not null and batter = t.player and result='FO' and runs_scored=1)
              where player=$player and season=$season and milr=$milr
        """
  }

  def getPlayerWithTypes(playerId: Int): Option[PlayerWithTypes] = awaitDB {
    PlayerWithTypesQuery
      .filter(_._1.id === playerId)
      .take(1)
      .result
      .headOption
  }.map(tuple2PlayerWithTypes)


  def getParkForTeam(team: Team): Park = awaitDB {
    Parks
      .filter(_.id === team.park)
      .take(1)
      .result
      .head
  }

  def getParkById(id: Int): Option[Park] = awaitDB {
    Parks
      .filter(_.id === id)
      .take(1)
      .result
      .headOption
  }

  def getPlayerForUser(user: User): Option[Player] = awaitDB {
    UserWithPlayerQuery
      .filter(_._1.id === user.id)
      .map(_._2)
      .take(1)
      .result
      .headOption
  }.flatten

  def getExtendedGameLog(id: Int): Seq[GameActionWithExtendedInfo] = awaitDB {
    GameActionsWithExtendedInfo
      .filter(_._1.gameId === id)
      .sortBy(_._1.id.desc)
      .result
  }.map(tuple2GameActionWithExtendedInfo)

  def getGameLog(game: Game): Seq[GameActionWithStatesAndPlayers] = awaitDB {
    GameActionsWithStatesAndPlayersQuery
      .filter(_._1.gameId === game.id)
      .sortBy(_._1.id.desc)
      .result
  }.map(tuple2GameActionWithStatesAndPlayers)

  def getScoringPlays(game: Game): Seq[ScoringPlay] = awaitDB {
    ScoringPlays
      .filter(_.gameId === game.id)
      .sortBy(_.gameAction.desc)
      .result
  }

  def getScoringPlaysWithActionsAndStates(game: Game): Seq[ScoringPlayWithActionAndStates] = awaitDB {
    ScoringPlaysWithActionsAndStatesQuery
      .filter(_._1.gameId === game.id)
      .sortBy(_._1.gameAction.desc)
      .result
  }.map(tuple2ScoringPlayWithActionAndStates)

  def setScoringPlayDescription(gameId: Int, gameAction: Int, newDescription: String): Unit = awaitDB {
    ScoringPlays
      .filter(sp => sp.gameId === gameId && sp.gameAction === gameAction)
      .map(_.description)
      .update(newDescription)
  }

  def getRecentGameActionWithPlayers(gameId: Int): Option[GameActionWithPlayers] = awaitDB {
    GameActionsWithPlayersQuery
      .filter(g => g._1.gameId === gameId && g._1.replacedBy.isEmpty)
      .take(1)
      .result
      .headOption
  }.map(tuple2GameActionWithPlayers)

  def deleteGameAction(play: GameAction, game: Game): Unit = {
    // Remove the replaced by reference on the prior play
    awaitDB(GameActions.filter(_.replacedBy === play.id).map(_.replacedBy).update(None))
    // Also remove the scoring play if applicable
    awaitDB(ScoringPlays.filter(_.gameAction === play.id).delete)
    // Update the game's state
    awaitDB(Games.filter(_.id === play.gameId).map(_.state).update(play.beforeState))
    // Delete the play
    awaitDB(GameActions.filter(_.id === play.id).delete)
    // Now delete the orphaned post-play state
    awaitDB(GameStates.filter(_.id === play.afterState).delete)

    // Finally, recalculate batting and pitching stats for the post-deletion-state players
    val newState = getGameStateById(play.beforeState).get
    if (game.statsCalculated) {
      if (game.statsCalculated) {
        // Collect unique hitters (batter + baserunners)
        Set(
          newState.r1,
          newState.r2,
          newState.r3,
          play.batter
        )
          .filter(_.isDefined)
          .map(_.get)
          .foreach(recalculateBattingStatsForPlayer(_, game.season, game.milr))

        // And unique pitchers (current and scorer placers)
        Set(
          newState.r1Responsibility,
          newState.r2Responsibility,
          newState.r3Responsibility,
          play.pitcher
        )
          .filter(_.isDefined)
          .map(_.get)
          .foreach(recalculatePitchingStatsForPlayer(_, game.season, game.milr))
      }
    }
  }

  def getGameStateById(id: Int): Option[GameState] = awaitDB {
    GameStates
      .filter(_.id === id)
      .take(1)
      .result
      .headOption
  }

  def getPlayerBattingPlays(player: Int, milr: Option[Boolean]): Seq[GameActionWithExtendedInfo] = awaitDB {
    milr.map { milr =>
      GameActionsWithExtendedInfo
        .filter(ga => ga._1.batter === player && ga._9.milr === milr)
        .result
    } getOrElse {
      GameActionsWithExtendedInfo
        .filter(_._1.batter === player)
        .result
    }
  }.map(tuple2GameActionWithExtendedInfo)

  def getPlayerPitchingPlays(player: Int, milr: Option[Boolean]): Seq[GameActionWithExtendedInfo] = awaitDB {
    milr.map { milr =>
      GameActionsWithExtendedInfo
        .filter(ga => ga._1.pitcher === player && ga._9.milr === milr)
        .result
    } getOrElse {
      GameActionsWithExtendedInfo
        .filter(_._1.pitcher === player)
        .result
    }
  }.map(tuple2GameActionWithExtendedInfo)

  def getMaxSession: (Int, Int) = {
    val maxSeason = awaitDB(Games.map(_.season).max.result).getOrElse(1)
    val maxSession = awaitDB(Games.filter(_.season === maxSeason).map(_.session).max.result).getOrElse(1)
    (maxSeason, maxSession)
  }

  def getSessionCounts: Map[Int, Int] = {
    awaitDB {
      Games
        .groupBy(g => g.season)
        .map { case (season, games) => season -> games.map(_.session).max }
        .result
    }.map(both => (both._1, both._2.getOrElse(1))).toMap
  }

  def editPlayer(form: EditPlayerForm): Unit = awaitDB {
    Players
      .filterNot(_.deactivated)
      .filter(_.id === form.id)
      .map(p => (p.name, p.firstName, p.lastName, p.battingType, p.pitchingType, p.pitchingBonus, p.righthanded, p.positionPrimary, p.positionSecondary, p.positionTertiary))
      .update((s"${form.firstName.getOrElse("")} ${form.lastName}".trim, form.firstName, form.lastName, form.battingType.get.id, form.pitchingType.map(_.id), form.pitchingBonus.map(_.id), form.rightHanded, form.positionPrimary, form.positionSecondary, form.positionTertiary))
  }

  def getAllPlayerNames: Seq[(Int, String)] = awaitDB {
    Players
      .filterNot(_.deactivated)
      .map(p => (p.id, p.name))
      .result
  }

  def getAllBattingTypes: Seq[BattingType] = awaitDB(BattingTypes.result)

  def getAllPitchingTypes: Seq[PitchingType] = awaitDB(PitchingTypes.result)

  def getAllPitchingBonuses: Seq[PitchingBonus] = awaitDB(PitchingBonuses.result)

  def logSignIn(user: Int, method: String, ip: String): Unit = awaitDB {
    SigninLogs.map(s => (s.user, s.method, s.ip)) += (user, method, ip)
  }

  def addGameAwards(game: Int, gameAwards: GameAwardsForm): Unit = awaitDB {
    GameAwards += new GameAwardsSet(game, gameAwards.winningPitcher, gameAwards.losingPitcher, gameAwards.playerOfTheGame, gameAwards.save)
  }

  def editGameAwards(game: Int, gameAwards: GameAwardsForm): Unit = awaitDB {
    GameAwards
      .filter(_.game === game)
      .map(ga => (ga.winningPitcher, ga.losingPitcher, ga.playerOfTheGame, ga.save))
      .update((gameAwards.winningPitcher, gameAwards.losingPitcher, gameAwards.playerOfTheGame, gameAwards.save))
  }

  def getGameAwards(game: Game): Option[GameAwardsWithPlayers] = awaitDB {
    GameAwardsWithPlayersQuery
      .filter(_._1.game === game.id)
      .take(1)
      .result
      .headOption
  }.map(tuple2GameAwardsWithPlayers)

  def createPark(form: EditParkForm): Int = awaitDB {
    Parks returning Parks.map(_.id) += new Park(0, form.name, form.factorHR, form.factor3B, form.factor2B, form.factor1B, form.factorBB)
  }

  def editPark(id: Int, form: EditParkForm): Unit = awaitDB {
    Parks
      .filter(_.id === id)
      .map(p => (p.name, p.factorHr, p.factor3b, p.factor2b, p.factor1b, p.factorBb))
      .update((form.name, form.factorHR, form.factor3B, form.factor2B, form.factor1B, form.factorBB))
  }

  def getAllPlayersWithUsers: Seq[PlayerWithUser] = awaitDB {
    PlayerWithUserQuery
      .filter(!_._1.deactivated)
      .result
  }.map(tuple2PlayerWithUser)

  def getGlobalSetting(name: String): Option[GlobalSetting] = awaitDB {
    GlobalSettings
      .filter(_.key === name)
      .take(1)
      .result
      .headOption
  }

  def setGlobalSetting(setting: GlobalSetting): Unit = awaitDB {
    GlobalSettings
      .insertOrUpdate(setting)
  }

  def addDiscordPing(pinger: User, receiver: User, game: Game, state: GameState): Unit = awaitDB {
    DiscordPings += new DiscordPing(0, pinger.id, receiver.id, game.id, state.id, new Timestamp(System.currentTimeMillis()))
  }

  def getDiscordPing(receiver: User, game: Game, state: GameState): Option[DiscordPing] = awaitDB {
    DiscordPings
      .filter(_.receiver === receiver.id)
      .filter(_.game === game.id)
      .filter(_.gameState === state.id)
      .take(1)
      .result
      .headOption
  }

  def addRedditPing(pinger: User, receiver: User, game: Game, state: GameState, status: Int, message: String, id36: Option[String]): Unit = awaitDB {
    RedditPings += new RedditPing(0, pinger.id, receiver.id, game.id, state.id, new Timestamp(System.currentTimeMillis()), status, message, id36)
  }

  def getRedditPing(receiver: User, game: Game, state: GameState): Option[RedditPing] = awaitDB {
    RedditPings
      .filter(_.receiver === receiver.id)
      .filter(_.game === game.id)
      .filter(_.gameState === state.id)
      .take(1)
      .result
      .headOption
  }

  def getGameStatusInfo(season: Int, session: Int): Seq[GameWithStatusInfo] = {
    val games = getGamesInSession(season, session)
    games.map { game =>
      val actions = getGameLog(game.game)
      tuple2GameWithStatusInfo((game, if (actions.nonEmpty) Some(actions.map(_.gameAction).maxBy(_.id)) else None, actions.length, getGameAwards(game.game).map(_.gameAwards)))
    }
  }

  def getChallongeBracketById(id: String): Option[ChallongeBracket] = awaitDB {
    ChallongeBrackets
      .filter(_.id === id)
      .take(1)
      .result
      .headOption
  }

  def getAllChallongeBracketNames: Seq[String] = awaitDB {
    ChallongeBrackets
      .map(_.id)
      .result
  }

  def deleteChallongeBracket(id: String): Unit = awaitDB {
    ChallongeBrackets
      .filter(_.id === id)
      .delete
  }

  def editOrCreateChallongeBracket(form: ChallongeBracketForm): Unit = awaitDB {
    ChallongeBrackets
      .insertOrUpdate(new ChallongeBracket(form.id, form.theme, form.multiplier, form.matchWidthMultiplier, form.scaleToFit, form.showFinalResults, form.showStandings, form.showVoting, form.tab))
  }

  def getDBMigrationVersion: String = awaitDB {
    FlywaySchemaHistory
      .sortBy(_.installedRank.desc)
      .take(1)
      .result
      .head
  }.version.get

  def addActionLog(reqPath: String, reqMethod: String, reqTime: Long, respTime: Long, respCode: Int, user: Option[Int], ip: String): Unit = awaitDB {
    ActionLogs.map(a => (a.requestPath, a.requestMethod, a.requestStamp, a.responseStamp, a.responseCode, a.requestingUser, a.srcIp)) += (reqPath, reqMethod, new Timestamp(reqTime), new Timestamp(respTime), respCode, user, ip)
  }

  def addUniqueAccessPoint(reqUser: Int, reqIp: String, reqDate: Date): Unit = awaitDB {
    sql"insert into unique_access_points (requesting_user, src_ip, req_date) values ($reqUser, $reqIp, $reqDate) on duplicate key update requesting_user=requesting_user;"
      .asUpdate
  }

  def submitVote(user: Int, form: VoteForm): Unit = awaitDB {
    S4s4Votes
      .insertOrUpdate(S4s4VotesRow(user, new Timestamp(System.currentTimeMillis), form.option1, form.option2, form.option3, form.option4, form.option5))
  }

  def getVote(user: Int): Option[Tables.S4s4VotesRow] = awaitDB {
    S4s4Votes
      .filter(_.user === user)
      .take(1)
      .result
      .headOption
  }

  def getVoteTally: Map[String, Int] = {
    VoteForm.candidates.map { s =>
      s -> awaitDB(S4s4Votes.filter(v => v.option1 === s || v.option2 === s || v.option3 === s || v.option4 === s || v.option5 === s).size.result)
    }.toMap
  }

  def setRefreshToken(userId: Int, refreshToken: Option[String]): Unit = awaitDB {
    Users
      .filter(_.id === userId)
      .map(_.refreshToken)
      .update(refreshToken)
  }

  def assignPlayerToTeam(player: Player, form: AssignPlayerToTeamForm): Unit = {
    awaitDB(
      Players
        .filter(_.id === player.id)
        .map(_.team)
        .update(form.team.map(_.id))
    )
    awaitDB(
      PlayerTeamAssignments
        .insertOrUpdate(PlayerTeamAssignmentsRow(player.id, form.team.map(_.id), form.startSeason, form.startSession))
    )
  }

}
