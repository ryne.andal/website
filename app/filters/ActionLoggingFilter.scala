package filters

import java.sql.Date

import akka.stream.Materializer
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.Logging
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ActionLoggingFilter @Inject()(db: FBDatabase)(implicit val mat: Materializer, ec: ExecutionContext) extends Filter with Logging {

  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    // Break early if path is excluded
    val path = requestHeader.path
    if (path.startsWith("/assets/") || path == "/favicon.ico")
      return nextFilter(requestHeader)

    val startTime = System.currentTimeMillis()
    val method = requestHeader.method
    val user = requestHeader.session.get("uid").map(_.toInt)
    val ip = requestHeader.headers.get("X-Real-IP").getOrElse(requestHeader.remoteAddress)

    nextFilter(requestHeader).map { result =>
      val endTime = System.currentTimeMillis()
      val code = result.header.status

      val message = s"$method ${requestHeader.uri} took ${endTime - startTime}ms returning $code"
      if (code >= 200 && code < 400)
        logger.info(message)
      else if (code >= 400 && code < 500) {
        logger.warn(message)
        db.addActionLog(path, method, startTime, endTime, code, user, ip)
      }
      else if (code >= 500 && code < 600) {
        logger.error(message)
        db.addActionLog(path, method, startTime, endTime, code, user, ip)
      }
      else
        logger.warn(s"UNKOWN RESPONSE TYPE: $message")

      user.foreach { user =>
        db.addUniqueAccessPoint(user, ip, new Date(startTime))
      }

      result
    }
  }

}
